var path = require('path');
module.exports = {
  entry: {
    app: ['./src/app.js'],
  },
  output: {
    //path: path.resolve(__dirname, 'public'),
    path: path.resolve(__dirname, 'build'),
    publicPath: '/',
    filename: '[name].js'
  },
  mode: 'development',
  devtool: 'source-map',
  devServer: {
    contentBase: path.join(__dirname, 'public'),
    compress: true,
    port: 3333
  },
  watchOptions: {
    ignored: [
      /node_modules([\\]+|\/)+(?!markdown_loader)/,
      /markdown_loader([\\]+|\/)node_modules/
    ]
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        include: /client/,
        //exclude: /node_modules/,
        use: 'babel-loader'
      },
      {
        test: /\.md$/,
        include: /page/,
        //exclude: /node_modules/,
        use: 'markdown_loader'
      },
      {
        test: /\.css$/,
        use: [{ loader: 'style-loader' }, { loader: 'css-loader' }],
      },
    ]
  }
};
