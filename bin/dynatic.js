#!/usr/bin/env node

var fs = require('fs');
var path = require('path');
var program = require('commander');

var convert = require('specdom_tools').convert;
var fix_link_path = require('specdom_tools').fix_link_path;
//var join = require('../src/join');

program
  .command('mk_specs [input_path_relative] [output_path_relative]')
  .description('convert markdown files to specdom JSON files, and joins them into one specs.json')
  //.option('-f, --output-format <output_format>', 'output format')
  //.option('-i, --input-path <input_path_option>', 'relative path to input directory')
  //.option('-o, --output-path <output_path_option>', 'relative path to output directory')
  .action((input_path_relative, output_path_relative) => {
    console.log('---');
    console.log('making specdom JSON files');
    //let run_path = path.resolve('./');
    input_path_relative = input_path_relative || '';
    //let input_path = path.join(run_path, input_path_relative);
    output_path_relative = output_path_relative || input_path_relative;
    //let output_path = path.join(run_path, output_path_relative);
    //convert(input_path, output_path);
    let pages = convert(input_path_relative);
    //join(input_path, output_path);
    //join(input_path_relative, output_path_relative);
    let output_json_path = path.join(output_path_relative, 'pages.json');
    let pages_json = JSON.stringify(pages);
    console.log('into: ' + output_json_path);
    fs.writeFileSync(output_json_path, pages_json);
  });
program
  .command('fix_link_path [input_path_relative] [output_path_relative]')
  .option('-p, --prepend-string <prepend_string>', 'string to prepend to href')
  .option('-c, --check-starts-with <starts_with>', 'prepend only if HREF starts with')
  .option('-t, --tag <tag>', 'prepend only if this tag')
  .option('-a, --assets_path <assets_path>', 'path to assets on server')
  .description('convert prepends text to href/src in specdom JSON file')
  .action((input_path_relative, output_path_relative, cmdObj) => {
    console.log('---');
    console.log('prepending links');
    input_path_relative = input_path_relative || '';
    output_path_relative = output_path_relative || input_path_relative;
    let input_file_path = path.join(input_path_relative, 'pages.json');
    let input_string = fs.readFileSync(input_file_path, { encoding: 'utf8' });
    let pages = JSON.parse(input_string);
    //Object.keys(pages).forEach( page_name => fix_link_path(pages[page_name],cmdObj.prependString,cmdObj.checkStartsWith,cmdObj.tag) );
    Object.keys(pages).forEach(page_name => {
      let path = './' + ['#'].concat(page_name.split('/').slice(0, -1)).join('/') + '/';
      pages[page_name] = fix_link_path(pages[page_name], { path });
    });
    let output_json_path = path.join(output_path_relative, 'pages.json');
    let pages_json = JSON.stringify(pages);
    console.log('into: ' + output_json_path);
    fs.writeFileSync(output_json_path, pages_json);
  });
program.parse(process.argv);



