#!/usr/bin/env node
const fs = require('fs');
const fse = require('fs-extra');
const path = require('path');
//const program = require('commander');
const markdown_to_specdom = require('specdom_tools').markdown_to_specdom;
const compact = require('specdom_tools').compact;
//const fix_link_path = require('specdom_tools').fix_link_path;
//const webpack = require('webpack');
//const webpack_config = require('../webpack.config.js');

var config = {};
try {
  config = require('../dynatic.config.js');
} catch (error) {
  console.log('No config file found...');
  console.log('...using defalt config.');
}
if (config.constructor === Function) {
  config = config();
}
config = Object.assign({
  build_path: 'build',
  markdown_input_path: 'pages',
  input_path_relative: '',
  output_path_relative: '',
  asset_paths: [
    ['static', 'build'],
  ],
  page_function: false,
  app_path: 'node_modules/dynatic/build/',
}, config);

console.log('---');
//"mk_specs": "npx dynatic mk_specs src/pages src/client;
console.log('getting pages');
function load_files(input_path, markdown_strings) {
  let extentions = ['md', 'markdown'];
  let files = fs.readdirSync(input_path);
  markdown_strings = markdown_strings || {};
  files.forEach(file_name => {
    let file_name_parts = file_name.split('.');
    let ext = file_name_parts.slice(-1)[0];
    let input_file_path = path.join(input_path, file_name);
    if (fs.lstatSync(input_file_path).isDirectory()) {
      console.log('opening: ' + input_file_path);
      load_files(input_file_path, markdown_strings);
    } else if (extentions.includes(ext)) {
      console.log('loading: ' + input_file_path);
      var page_name = input_file_path.split(/\/|\./).slice(-2)[0];
      var location = input_file_path.split(/\/|\./).slice(1, -2);
      let file_string = fs.readFileSync(input_file_path, { encoding: 'utf8' });
      var page_id;
      if (location.length > 0) {
        page_id = location.join('/') + '/' + page_name;
      } else {
        page_id = page_name;
      }
      markdown_strings[page_id] = file_string;
    }
  });
  return markdown_strings;
}
let markdown_strings = load_files(config.markdown_input_path);
let pages = markdown_to_specdom(markdown_strings);

if (config.page_function) {
  Object.keys(pages).forEach(page_name => {
    console.log('Using supplied function on pages:', page_name);
    pages[page_name] = config.page_function(pages[page_name]);
  });
}
////////////////
console.log('Clearing build...');
fse.emptyDirSync(config.build_path);
///////////
console.log('saving json');
Object.keys(pages).forEach(page_name => {
  console.log('compacting:', page_name);
  pages[page_name] = compact(pages[page_name]);
});
let pages_json = JSON.stringify(pages);
let output_json_path = path.join(config.build_path, 'pages.json');
console.log('into: ' + output_json_path);
fs.writeFileSync(output_json_path, pages_json);


console.log('Copying asset files to build...');
if (config.asset_paths.constructor === String) config.asset_paths = [config.asset_paths];
config.asset_paths.forEach(asset_path => {
  fse.copySync(asset_path[0], asset_path[1]);
});
///////////
console.log('installing app');
[
  'index.html',
  'app.js',
  'app.js.map',
].forEach(file_name => {
  fse.copySync(path.join(config.app_path, file_name), path.join(config.build_path, file_name));
});
