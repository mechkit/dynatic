/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./index.js":
/*!******************!*\
  !*** ./index.js ***!
  \******************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return dynatic; });
/* harmony import */ var normalize_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! normalize.css */ "./node_modules/normalize.css/normalize.css");
/* harmony import */ var normalize_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(normalize_css__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var specdom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! specdom */ "./node_modules/specdom/index.js");
/* harmony import */ var specdom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(specdom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var hash_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! hash_router */ "./node_modules/hash_router/index.js");
/* harmony import */ var state_manager__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! state_manager */ "./node_modules/state_manager/index.js");
/* harmony import */ var _src_reducers__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./src/reducers */ "./src/reducers.js");
/**
* this is the app
* @file_overview this the starting point for the application.
* @author keith showalter {@link https://github.com/kshowalter}
* @version 0.1.0
*/





//import require_pages from './require_pages';
function dynatic(pages, settings) {
  settings = settings || {};
  //var context = require.context( MD_DIR, true, /\.md$/);
  //var pages = require_pages(context);
  let menu_items = false;
  let menu_spec_c = false;
  let title = false;
  let title_spec_c = false;
  let default_page_name = 'default';
  if (pages['home'] !== undefined) {
    default_page_name = 'home';
  }
  if (pages['menu']) {
    let menu_md_specs_c = pages['menu'].children[0].children;
    if (menu_md_specs_c[0].tag === 'h1') {
      title_spec_c = menu_md_specs_c[0].children;
      if (menu_md_specs_c[1] && menu_md_specs_c[1].tag === 'ul') {
        menu_spec_c = menu_md_specs_c[1].children;
      }
    }
    if (menu_md_specs_c[0].tag === 'li') {
      menu_spec_c = menu_md_specs_c;
    }
    if (title_spec_c) {
      title = title_spec_c;
    }
    if (menu_spec_c) {
      menu_items = menu_spec_c.map(
        li_spec => {
          return {
            name: li_spec.children[0].children[0].children[0],
            href: li_spec.children[0].children[0].props.href,
          };
        }
      );
    }
  }
  pages['_page_index'] = {
    tag: 'div',
    class: 'level_0',
    id: 'page',
    meta: {
      location: [],
      name: '_page_index',
      title: 'Page Index',
    },
    children: {
      tag: 'div',
      class: 'section level_1',
      children: {
        tag: 'div',
        class: 'section level_2',
        children: {
          tag: 'ul',
          children: Object.keys(pages).map(page_name => {
            return {
              tag: 'li',
              children: {
                tag: 'a',
                text: pages[page_name].meta.title + ' (' + pages[page_name].meta.page_id + ')',
                href: '#/' + page_name,
              },
            };
          })
        }
      }
    }
  };
  console.log('pages', pages);
  var init_state = {
    title: title || settings.title || 'website',
    menu_items: menu_items || settings.menu_items || [],
    pages: pages,
    default_page_name: settings.default_page_name || default_page_name,
    selected_page_id: '',
    page_content: {},
    specs: {},
    body_style: {},
  };
  var sdom = specdom__WEBPACK_IMPORTED_MODULE_1___default()('body');
  var actions = Object(state_manager__WEBPACK_IMPORTED_MODULE_3__["default"])(init_state, _src_reducers__WEBPACK_IMPORTED_MODULE_4__["default"], function (state) {
    // console.log('SPECS', state.specs);
    document.title = state.title;
    sdom.clear().append(state.specs);
    window.scrollTo(0, 0);
    // apply top level sytle to document.body
    if (state.page_content.style) {
      Object.keys(state.body_style).forEach(k => state.body_style[k] = null);
      state.body_style = Object.assign(state.body_style, state.page_content.style);
      Object.keys(state.body_style).forEach(k => {
        document.body.style[k] = state.body_style[k];
      });
    }
    if (settings.onload) {
      console.log('Starting .onload');
      settings.onload();
    }
  });
  var router = Object(hash_router__WEBPACK_IMPORTED_MODULE_2__["default"])(function (selection) {
    actions.load_page(selection);
  });
  router();
}


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./node_modules/normalize.css/normalize.css":
/*!****************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./node_modules/normalize.css/normalize.css ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "/*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */\n\n/* Document\n   ========================================================================== */\n\n/**\n * 1. Correct the line height in all browsers.\n * 2. Prevent adjustments of font size after orientation changes in iOS.\n */\n\nhtml {\n  line-height: 1.15; /* 1 */\n  -webkit-text-size-adjust: 100%; /* 2 */\n}\n\n/* Sections\n   ========================================================================== */\n\n/**\n * Remove the margin in all browsers.\n */\n\nbody {\n  margin: 0;\n}\n\n/**\n * Render the `main` element consistently in IE.\n */\n\nmain {\n  display: block;\n}\n\n/**\n * Correct the font size and margin on `h1` elements within `section` and\n * `article` contexts in Chrome, Firefox, and Safari.\n */\n\nh1 {\n  font-size: 2em;\n  margin: 0.67em 0;\n}\n\n/* Grouping content\n   ========================================================================== */\n\n/**\n * 1. Add the correct box sizing in Firefox.\n * 2. Show the overflow in Edge and IE.\n */\n\nhr {\n  box-sizing: content-box; /* 1 */\n  height: 0; /* 1 */\n  overflow: visible; /* 2 */\n}\n\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\n\npre {\n  font-family: monospace, monospace; /* 1 */\n  font-size: 1em; /* 2 */\n}\n\n/* Text-level semantics\n   ========================================================================== */\n\n/**\n * Remove the gray background on active links in IE 10.\n */\n\na {\n  background-color: transparent;\n}\n\n/**\n * 1. Remove the bottom border in Chrome 57-\n * 2. Add the correct text decoration in Chrome, Edge, IE, Opera, and Safari.\n */\n\nabbr[title] {\n  border-bottom: none; /* 1 */\n  text-decoration: underline; /* 2 */\n  text-decoration: underline dotted; /* 2 */\n}\n\n/**\n * Add the correct font weight in Chrome, Edge, and Safari.\n */\n\nb,\nstrong {\n  font-weight: bolder;\n}\n\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\n\ncode,\nkbd,\nsamp {\n  font-family: monospace, monospace; /* 1 */\n  font-size: 1em; /* 2 */\n}\n\n/**\n * Add the correct font size in all browsers.\n */\n\nsmall {\n  font-size: 80%;\n}\n\n/**\n * Prevent `sub` and `sup` elements from affecting the line height in\n * all browsers.\n */\n\nsub,\nsup {\n  font-size: 75%;\n  line-height: 0;\n  position: relative;\n  vertical-align: baseline;\n}\n\nsub {\n  bottom: -0.25em;\n}\n\nsup {\n  top: -0.5em;\n}\n\n/* Embedded content\n   ========================================================================== */\n\n/**\n * Remove the border on images inside links in IE 10.\n */\n\nimg {\n  border-style: none;\n}\n\n/* Forms\n   ========================================================================== */\n\n/**\n * 1. Change the font styles in all browsers.\n * 2. Remove the margin in Firefox and Safari.\n */\n\nbutton,\ninput,\noptgroup,\nselect,\ntextarea {\n  font-family: inherit; /* 1 */\n  font-size: 100%; /* 1 */\n  line-height: 1.15; /* 1 */\n  margin: 0; /* 2 */\n}\n\n/**\n * Show the overflow in IE.\n * 1. Show the overflow in Edge.\n */\n\nbutton,\ninput { /* 1 */\n  overflow: visible;\n}\n\n/**\n * Remove the inheritance of text transform in Edge, Firefox, and IE.\n * 1. Remove the inheritance of text transform in Firefox.\n */\n\nbutton,\nselect { /* 1 */\n  text-transform: none;\n}\n\n/**\n * Correct the inability to style clickable types in iOS and Safari.\n */\n\nbutton,\n[type=\"button\"],\n[type=\"reset\"],\n[type=\"submit\"] {\n  -webkit-appearance: button;\n}\n\n/**\n * Remove the inner border and padding in Firefox.\n */\n\nbutton::-moz-focus-inner,\n[type=\"button\"]::-moz-focus-inner,\n[type=\"reset\"]::-moz-focus-inner,\n[type=\"submit\"]::-moz-focus-inner {\n  border-style: none;\n  padding: 0;\n}\n\n/**\n * Restore the focus styles unset by the previous rule.\n */\n\nbutton:-moz-focusring,\n[type=\"button\"]:-moz-focusring,\n[type=\"reset\"]:-moz-focusring,\n[type=\"submit\"]:-moz-focusring {\n  outline: 1px dotted ButtonText;\n}\n\n/**\n * Correct the padding in Firefox.\n */\n\nfieldset {\n  padding: 0.35em 0.75em 0.625em;\n}\n\n/**\n * 1. Correct the text wrapping in Edge and IE.\n * 2. Correct the color inheritance from `fieldset` elements in IE.\n * 3. Remove the padding so developers are not caught out when they zero out\n *    `fieldset` elements in all browsers.\n */\n\nlegend {\n  box-sizing: border-box; /* 1 */\n  color: inherit; /* 2 */\n  display: table; /* 1 */\n  max-width: 100%; /* 1 */\n  padding: 0; /* 3 */\n  white-space: normal; /* 1 */\n}\n\n/**\n * Add the correct vertical alignment in Chrome, Firefox, and Opera.\n */\n\nprogress {\n  vertical-align: baseline;\n}\n\n/**\n * Remove the default vertical scrollbar in IE 10+.\n */\n\ntextarea {\n  overflow: auto;\n}\n\n/**\n * 1. Add the correct box sizing in IE 10.\n * 2. Remove the padding in IE 10.\n */\n\n[type=\"checkbox\"],\n[type=\"radio\"] {\n  box-sizing: border-box; /* 1 */\n  padding: 0; /* 2 */\n}\n\n/**\n * Correct the cursor style of increment and decrement buttons in Chrome.\n */\n\n[type=\"number\"]::-webkit-inner-spin-button,\n[type=\"number\"]::-webkit-outer-spin-button {\n  height: auto;\n}\n\n/**\n * 1. Correct the odd appearance in Chrome and Safari.\n * 2. Correct the outline style in Safari.\n */\n\n[type=\"search\"] {\n  -webkit-appearance: textfield; /* 1 */\n  outline-offset: -2px; /* 2 */\n}\n\n/**\n * Remove the inner padding in Chrome and Safari on macOS.\n */\n\n[type=\"search\"]::-webkit-search-decoration {\n  -webkit-appearance: none;\n}\n\n/**\n * 1. Correct the inability to style clickable types in iOS and Safari.\n * 2. Change font properties to `inherit` in Safari.\n */\n\n::-webkit-file-upload-button {\n  -webkit-appearance: button; /* 1 */\n  font: inherit; /* 2 */\n}\n\n/* Interactive\n   ========================================================================== */\n\n/*\n * Add the correct display in Edge, IE 10+, and Firefox.\n */\n\ndetails {\n  display: block;\n}\n\n/*\n * Add the correct display in all browsers.\n */\n\nsummary {\n  display: list-item;\n}\n\n/* Misc\n   ========================================================================== */\n\n/**\n * Add the correct display in IE 10+.\n */\n\ntemplate {\n  display: none;\n}\n\n/**\n * Add the correct display in IE 10.\n */\n\n[hidden] {\n  display: none;\n}\n", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/api.js":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
// eslint-disable-next-line func-names
module.exports = function (useSourceMap) {
  var list = []; // return the list of modules as css string

  list.toString = function toString() {
    return this.map(function (item) {
      var content = cssWithMappingToString(item, useSourceMap);

      if (item[2]) {
        return "@media ".concat(item[2], " {").concat(content, "}");
      }

      return content;
    }).join('');
  }; // import a list of modules into the list
  // eslint-disable-next-line func-names


  list.i = function (modules, mediaQuery, dedupe) {
    if (typeof modules === 'string') {
      // eslint-disable-next-line no-param-reassign
      modules = [[null, modules, '']];
    }

    var alreadyImportedModules = {};

    if (dedupe) {
      for (var i = 0; i < this.length; i++) {
        // eslint-disable-next-line prefer-destructuring
        var id = this[i][0];

        if (id != null) {
          alreadyImportedModules[id] = true;
        }
      }
    }

    for (var _i = 0; _i < modules.length; _i++) {
      var item = [].concat(modules[_i]);

      if (dedupe && alreadyImportedModules[item[0]]) {
        // eslint-disable-next-line no-continue
        continue;
      }

      if (mediaQuery) {
        if (!item[2]) {
          item[2] = mediaQuery;
        } else {
          item[2] = "".concat(mediaQuery, " and ").concat(item[2]);
        }
      }

      list.push(item);
    }
  };

  return list;
};

function cssWithMappingToString(item, useSourceMap) {
  var content = item[1] || ''; // eslint-disable-next-line prefer-destructuring

  var cssMapping = item[3];

  if (!cssMapping) {
    return content;
  }

  if (useSourceMap && typeof btoa === 'function') {
    var sourceMapping = toComment(cssMapping);
    var sourceURLs = cssMapping.sources.map(function (source) {
      return "/*# sourceURL=".concat(cssMapping.sourceRoot || '').concat(source, " */");
    });
    return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
  }

  return [content].join('\n');
} // Adapted from convert-source-map (MIT)


function toComment(sourceMap) {
  // eslint-disable-next-line no-undef
  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
  var data = "sourceMappingURL=data:application/json;charset=utf-8;base64,".concat(base64);
  return "/*# ".concat(data, " */");
}

/***/ }),

/***/ "./node_modules/functions/index.js":
/*!*****************************************!*\
  !*** ./node_modules/functions/index.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var f = {};

f.mk_sheet_num = {};
f.mk_report_page_num = {};
f.mk_preview = {};

var logger = {
  info: console.log,
}

f.setup_body = function(title, sections){
  document.title = title;
  var body = document.body;
  var status_bar = document.createElement('div');
  status_bar.id = 'status';
  status_bar.innerHTML = 'loading status...';
  body.insertBefore(status_bar, body.firstChild);
};

f.pad_zero = function(num, size){
  var s = '000000000' + num;
  return s.substr(s.length-size);
};

f.obj_names = function( object ) {
  if( object !== undefined ) {
    var a = [];
    for( var id in object ) {
      if( object.hasOwnProperty(id) )  {
        a.push(id);
      }
    }
    return a;
  }
};

f.object_defined = function(object){
  //logger.info(object);
  for( var key in object ){
    if( object.hasOwnProperty(key) ){
      //logger.info(key);
      if( object[key] === null || object[key] === undefined ) return false;
    }
  }
  return true;
};

f.nullToObject = function(object){
  for( var key in object ){
    if( object.hasOwnProperty(key) ){
      if( object[key] === null ){
        object[key] = {};
      } else if( typeof object[key] === 'object' ) {
        object[key] = f.nullToObject(object[key]);
      }
    }
  }
  return object;
};

f.blank_copy = function(object){
  var newObject = {};
  for( var key in object ){
    if( object.hasOwnProperty(key) ){
      if( object[key].constructor === Object ) {
        newObject[key] = {};
        for( var key2 in object[key] ){
          if( object[key].hasOwnProperty(key2) ){
            newObject[key][key2] = null;
          }
        }
      } else {
        newObject[key] = null;
      }
    }
  }
  return newObject;
};

f.blank_clean_copy = function(object){
  var newObject = {};
  for( var key in object ){
    if( object.hasOwnProperty(key) ){
      if( object[key].constructor === Object ) {
        newObject[key] = {};
        for( var key2 in object[key] ){
          if( object[key].hasOwnProperty(key2) ){
            var clean_key = f.clean_name(key2);
            newObject[key][clean_key] = null;
          }
        }
      } else {
        newObject[key] = null;
      }
    }
  }
  return newObject;
};

f.merge_objects = function merge_objects(object1, object2){
  for( var key in object1 ){
    if( object1.hasOwnProperty(key) ){
      //if( key === 'make' ) logger.info(key, object1, typeof object1[key], typeof object2[key]);
      //logger.info(key, object1, typeof object1[key], typeof object2[key]);
      if( object1[key] && object1[key].constructor === Object ) {
        if( object2[key] === undefined ) object2[key] = {};
        merge_objects( object1[key], object2[key] );
      } else {
        object2[key] = object1[key];
      }
    }
  }
};

f.array_to_object = function(arr) {
  var r = {};
  for (var i = 0; i < arr.length; ++i)
  r[i] = arr[i];
  return r;
};

f.nan_check = function nan_check(object, path){
  if( path === undefined ) path = "";
  path = path+".";
  for( var key in object ){
    //logger.info( "NaNcheck: "+path+key );

    if( object[key] && object[key].constructor === Array ) object[key] = f.array_to_object(object[key]);


    if(  object[key] && ( object.hasOwnProperty(key) || object[key] !== null )){
      if( object[key].constructor === Object ){
        //logger.info( "  Object: "+path+key );
        nan_check( object[key], path+key );
      } else if( object[key] === NaN || object[key] === null ){
        logger.info( "NaN: "+path+key );
      } else {
        //logger.info( "Defined: "+path+key, object[key]);

      }
    }

  }
};

f.str_to_num = function str_to_num(input){
  if(input==='') return input;
  var output;
  if(!isNaN(input)) output = Number(input);
  else output = input;
  return output;
};


f.pretty_word = function(name){
  return name.charAt(0).toUpperCase() + name.slice(1);
};

f.to_underscore = function(name){
  return name.replace(/([A-Z])/g, function($1){
    return '_'+$1.toLowerCase();
  });
};

f.pretty_name = function(name){
  name = f.to_underscore(name);
  var l = name.split('_');
  l.forEach(function(name_seqment,i){
    l[i] = f.pretty_word(name_seqment);
  });
  var pretty = l.join(' ');

  return pretty;
};


f.pretty_names = function(object){
  var new_object = {};
  for( var key in object ){
    if( object.hasOwnProperty(key) ){
      var new_key = f.pretty_name(key);
      new_object[new_key] = object[key];
    }
  }
  return new_object;
};
f.name_to_id = function(name){
  return name.replace(' ','_').replace(/\W/g, '_').toLowerCase();
};

f.clean_name = function(name){
  return name.split(' ')[0];
};

f.format_value = function(value_in){
  var value_out;

  if( typeof value_in === 'undefined' || value_in === null ) {
    value_out = false;
  } else if( value_in.constructor === Array ){
    value_out = value_in.join(', ');
  } else if( value_in.constructor === Object ){
    value_out = false;
  } else if( ( value_in && value_in.constructor === String && value_in.trim() === '' ) || isNaN(value_in) ){
    value_out = value_in;
  } else {
    var value_float = parseFloat(value_in);
    if( (value_float%1) === 0 ){ // if is intiger
      value_out = parseFloat(value_in).toFixed(0);
    } else {
      value_out = parseFloat(value_in).toFixed(2);
    }
  }

  return value_out;
};


f.display_value = function(value_in){
  var value_out = f.format_value(value_in);

  if( value_out.constructor === String ){
    value_out = value_out;
  } else if( value_out === false || isNaN(value_out) ){
    value_out = '';
  }

  return value_out;
};

f.lowercase_properties = function lowercase_properties(obj) {
  var new_object = new obj.constructor();
  for( var old_name in obj ){
    if (obj.hasOwnProperty(old_name)) {
      var new_name = old_name.toLowerCase();
      if(obj[old_name] && ( obj[old_name].constructor === Object || obj[old_name].constructor === Array )){
        new_object[new_name] = lowercase_properties(obj[old_name]);
      } else {
        new_object[new_name] = obj[old_name];
      }
    }

  }
  return new_object;
};

f.clear_object = function(obj){
  for( var id in obj ){
    if( obj.hasOwnProperty(id)){
      delete obj[id];
    }
  }
};


f.are_we_there_yet = function are_we_there_yet(test, done, fail ){
  if( test() ){
    //logger.info('test: PASS');
    done();
  } else {
    //logger.info('test: fail');
    //*
    // may need polyfill for IE9
    // https://developer.mozilla.org/en-US/docs/Web/API/WindowTimers/setTimeout
    if(fail) fail();
    setTimeout(
      are_we_there_yet,
      10,
      test,
      done,
      fail
    );
    //*/
  }
};

f.mk_ready = function(names, callback){
  var list = {};
  var data_collection = {};
  names.forEach(function(name){
    list[name] = false;
  });
  var ready = false;

  return function(name, data){
    if( name === false ){
      callback({error:false});
    }
    //logger.info('name:', name);
    if(name){
      list[name] = true;
      if(data){
        data_collection[name] = data;
      }
    }
    for( name in list){
      if( list[name] === false ){
        return false;
      }
    }
    //logger.info('ready!!!!!', list);
    if(callback){
      callback(data_collection);
    }
    return true;
  };
};

f.mk_ready_count = function(total_count, callback){
  var count = 0;
  var data_collection = {};
  var ready = false;

  return function(data, err){
    count++;
    if( data === false ){
      logger.info('failure state');
      callback(false);
    }
    if( data || err ){
      data_collection[count] = {
        data: data,
        err: err
      };
    }
    if( count < total_count ){
      return false;
    }
    if(callback){
      callback(data_collection);
    }
    return true;
  };
};


f.split_long_sentence = function(string, length){
  var lines = [];
  while( string.length >= length ){
    var i = length;
    while( string[i] !== ' ' ){
      i--;
    }
    lines.push(string.slice(0,i));
    string = string.slice(i+1);
  }
  lines.push(string);
  return lines;
};

f.get_max_of_array = function(num_array) {
  return Math.max.apply(null, num_array);
};
f.get_min_of_array = function(num_array) {
  return Math.min.apply(null, num_array);
};

f.range = function(num) {
  return Array.apply(null, Array(num)).map(function ($, i) {return i;});
};

f.mk_range_measurment = function(){
  var range = [];

  var check = function(value){
    range[0] = range[0] || value;
    range[1] = range[1] || value;
    if( value < range[0] ){
      range[0] = value;
    }
    if( value > range[1] ){
      range[1] = value;
    }
    return range;
  };

  return check;
};

f.format_milliseconds = function(milliseconds_number){
  var seconds_number = milliseconds_number/1000;
  var days    = Math.floor( seconds_number / (3600*24) )
  var hours   = Math.floor( ( seconds_number - (days*(3600*24))                          ) / 3600 );
  var minutes = Math.floor( ( seconds_number - (days*(3600*24)) - (hours * 3600)         ) / 60   );
  var seconds = seconds_number - (days*(3600*24)) - (hours * 3600) - (minutes * 60);
  var seconds = Math.floor( seconds*1000 )/1000;

  if (days    < 10) {days    = '0'+days;}
  if (hours   < 10) {hours   = '0'+hours;}
  if (minutes < 10) {minutes = '0'+minutes;}
  if (seconds < 10) {seconds = '0'+seconds;}

  return days+'d '+hours+'h '+minutes+'m '+seconds+'s';
}

f.split_string_by_length = function(str,length){
  if(str.length < length ){
    return [str];
  } else {
    var str_array = [];
    while(str.length > length){
      var i = length;
      while( str.length > i && str[i] !== ' ' ){
        i--;
      }
      str_array.push(str.slice(0,i));
      str = str.slice(i+1);
    }
    str_array.push(str);
    return str_array;
  }

}

module.exports = f;
//export default f;


/***/ }),

/***/ "./node_modules/hash_router/index.js":
/*!*******************************************!*\
  !*** ./node_modules/hash_router/index.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (function(callback){

  function hash_check(callback) {
    if( location.hash === '' || location.hash === '#' || location.hash === '#/' ){
      //window.location.hash = '#/';
      callback(false);
    } else {
      //console.log('location.hash',location.hash);
      var url = location.hash.slice(2) || '/';
      //console.log('url',url);
      var values = url.split('/');

      callback(values);

    }
  }

  // Listen on hash change:
  window.addEventListener('hashchange', function(){
    hash_check(callback);
  });
  // Listen on page load:
  //window.addEventListener('load', function(){
  //  hash_check(callback);
  //});

  return function(new_route){
    if( new_route && new_route.constructor === String ){
      window.location.hash = '#/' + new_route;
      //hash_check(callback);
    } else {
      hash_check(callback);
      //console.warn('new route is not a string');
    }
  };

});


/***/ }),

/***/ "./node_modules/lodash-es/_Symbol.js":
/*!*******************************************!*\
  !*** ./node_modules/lodash-es/_Symbol.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _root_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./_root.js */ "./node_modules/lodash-es/_root.js");


/** Built-in value references. */
var Symbol = _root_js__WEBPACK_IMPORTED_MODULE_0__["default"].Symbol;

/* harmony default export */ __webpack_exports__["default"] = (Symbol);


/***/ }),

/***/ "./node_modules/lodash-es/_baseGetTag.js":
/*!***********************************************!*\
  !*** ./node_modules/lodash-es/_baseGetTag.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Symbol_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./_Symbol.js */ "./node_modules/lodash-es/_Symbol.js");
/* harmony import */ var _getRawTag_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./_getRawTag.js */ "./node_modules/lodash-es/_getRawTag.js");
/* harmony import */ var _objectToString_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./_objectToString.js */ "./node_modules/lodash-es/_objectToString.js");




/** `Object#toString` result references. */
var nullTag = '[object Null]',
    undefinedTag = '[object Undefined]';

/** Built-in value references. */
var symToStringTag = _Symbol_js__WEBPACK_IMPORTED_MODULE_0__["default"] ? _Symbol_js__WEBPACK_IMPORTED_MODULE_0__["default"].toStringTag : undefined;

/**
 * The base implementation of `getTag` without fallbacks for buggy environments.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the `toStringTag`.
 */
function baseGetTag(value) {
  if (value == null) {
    return value === undefined ? undefinedTag : nullTag;
  }
  return (symToStringTag && symToStringTag in Object(value))
    ? Object(_getRawTag_js__WEBPACK_IMPORTED_MODULE_1__["default"])(value)
    : Object(_objectToString_js__WEBPACK_IMPORTED_MODULE_2__["default"])(value);
}

/* harmony default export */ __webpack_exports__["default"] = (baseGetTag);


/***/ }),

/***/ "./node_modules/lodash-es/_freeGlobal.js":
/*!***********************************************!*\
  !*** ./node_modules/lodash-es/_freeGlobal.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/** Detect free variable `global` from Node.js. */
var freeGlobal = typeof global == 'object' && global && global.Object === Object && global;

/* harmony default export */ __webpack_exports__["default"] = (freeGlobal);

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./node_modules/lodash-es/_getPrototype.js":
/*!*************************************************!*\
  !*** ./node_modules/lodash-es/_getPrototype.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _overArg_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./_overArg.js */ "./node_modules/lodash-es/_overArg.js");


/** Built-in value references. */
var getPrototype = Object(_overArg_js__WEBPACK_IMPORTED_MODULE_0__["default"])(Object.getPrototypeOf, Object);

/* harmony default export */ __webpack_exports__["default"] = (getPrototype);


/***/ }),

/***/ "./node_modules/lodash-es/_getRawTag.js":
/*!**********************************************!*\
  !*** ./node_modules/lodash-es/_getRawTag.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Symbol_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./_Symbol.js */ "./node_modules/lodash-es/_Symbol.js");


/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var nativeObjectToString = objectProto.toString;

/** Built-in value references. */
var symToStringTag = _Symbol_js__WEBPACK_IMPORTED_MODULE_0__["default"] ? _Symbol_js__WEBPACK_IMPORTED_MODULE_0__["default"].toStringTag : undefined;

/**
 * A specialized version of `baseGetTag` which ignores `Symbol.toStringTag` values.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the raw `toStringTag`.
 */
function getRawTag(value) {
  var isOwn = hasOwnProperty.call(value, symToStringTag),
      tag = value[symToStringTag];

  try {
    value[symToStringTag] = undefined;
    var unmasked = true;
  } catch (e) {}

  var result = nativeObjectToString.call(value);
  if (unmasked) {
    if (isOwn) {
      value[symToStringTag] = tag;
    } else {
      delete value[symToStringTag];
    }
  }
  return result;
}

/* harmony default export */ __webpack_exports__["default"] = (getRawTag);


/***/ }),

/***/ "./node_modules/lodash-es/_objectToString.js":
/*!***************************************************!*\
  !*** ./node_modules/lodash-es/_objectToString.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/** Used for built-in method references. */
var objectProto = Object.prototype;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var nativeObjectToString = objectProto.toString;

/**
 * Converts `value` to a string using `Object.prototype.toString`.
 *
 * @private
 * @param {*} value The value to convert.
 * @returns {string} Returns the converted string.
 */
function objectToString(value) {
  return nativeObjectToString.call(value);
}

/* harmony default export */ __webpack_exports__["default"] = (objectToString);


/***/ }),

/***/ "./node_modules/lodash-es/_overArg.js":
/*!********************************************!*\
  !*** ./node_modules/lodash-es/_overArg.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/**
 * Creates a unary function that invokes `func` with its argument transformed.
 *
 * @private
 * @param {Function} func The function to wrap.
 * @param {Function} transform The argument transform.
 * @returns {Function} Returns the new function.
 */
function overArg(func, transform) {
  return function(arg) {
    return func(transform(arg));
  };
}

/* harmony default export */ __webpack_exports__["default"] = (overArg);


/***/ }),

/***/ "./node_modules/lodash-es/_root.js":
/*!*****************************************!*\
  !*** ./node_modules/lodash-es/_root.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _freeGlobal_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./_freeGlobal.js */ "./node_modules/lodash-es/_freeGlobal.js");


/** Detect free variable `self`. */
var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

/** Used as a reference to the global object. */
var root = _freeGlobal_js__WEBPACK_IMPORTED_MODULE_0__["default"] || freeSelf || Function('return this')();

/* harmony default export */ __webpack_exports__["default"] = (root);


/***/ }),

/***/ "./node_modules/lodash-es/isObjectLike.js":
/*!************************************************!*\
  !*** ./node_modules/lodash-es/isObjectLike.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/**
 * Checks if `value` is object-like. A value is object-like if it's not `null`
 * and has a `typeof` result of "object".
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
 * @example
 *
 * _.isObjectLike({});
 * // => true
 *
 * _.isObjectLike([1, 2, 3]);
 * // => true
 *
 * _.isObjectLike(_.noop);
 * // => false
 *
 * _.isObjectLike(null);
 * // => false
 */
function isObjectLike(value) {
  return value != null && typeof value == 'object';
}

/* harmony default export */ __webpack_exports__["default"] = (isObjectLike);


/***/ }),

/***/ "./node_modules/lodash-es/isPlainObject.js":
/*!*************************************************!*\
  !*** ./node_modules/lodash-es/isPlainObject.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _baseGetTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./_baseGetTag.js */ "./node_modules/lodash-es/_baseGetTag.js");
/* harmony import */ var _getPrototype_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./_getPrototype.js */ "./node_modules/lodash-es/_getPrototype.js");
/* harmony import */ var _isObjectLike_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./isObjectLike.js */ "./node_modules/lodash-es/isObjectLike.js");




/** `Object#toString` result references. */
var objectTag = '[object Object]';

/** Used for built-in method references. */
var funcProto = Function.prototype,
    objectProto = Object.prototype;

/** Used to resolve the decompiled source of functions. */
var funcToString = funcProto.toString;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/** Used to infer the `Object` constructor. */
var objectCtorString = funcToString.call(Object);

/**
 * Checks if `value` is a plain object, that is, an object created by the
 * `Object` constructor or one with a `[[Prototype]]` of `null`.
 *
 * @static
 * @memberOf _
 * @since 0.8.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a plain object, else `false`.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 * }
 *
 * _.isPlainObject(new Foo);
 * // => false
 *
 * _.isPlainObject([1, 2, 3]);
 * // => false
 *
 * _.isPlainObject({ 'x': 0, 'y': 0 });
 * // => true
 *
 * _.isPlainObject(Object.create(null));
 * // => true
 */
function isPlainObject(value) {
  if (!Object(_isObjectLike_js__WEBPACK_IMPORTED_MODULE_2__["default"])(value) || Object(_baseGetTag_js__WEBPACK_IMPORTED_MODULE_0__["default"])(value) != objectTag) {
    return false;
  }
  var proto = Object(_getPrototype_js__WEBPACK_IMPORTED_MODULE_1__["default"])(value);
  if (proto === null) {
    return true;
  }
  var Ctor = hasOwnProperty.call(proto, 'constructor') && proto.constructor;
  return typeof Ctor == 'function' && Ctor instanceof Ctor &&
    funcToString.call(Ctor) == objectCtorString;
}

/* harmony default export */ __webpack_exports__["default"] = (isPlainObject);


/***/ }),

/***/ "./node_modules/markdown_specs/get_props.js":
/*!**************************************************!*\
  !*** ./node_modules/markdown_specs/get_props.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var special_chars = __webpack_require__(/*! ./special_chars */ "./node_modules/markdown_specs/special_chars.js");

module.exports = function get_props(string){
  //console.log(string)
  string = string || '';
  var desc_parts = {
    text: string,
    props: {},
    style: {}
  };
  var mark = string.length-1;
  while( mark > -1 ){
    if( [')'].indexOf(string[mark]) !== -1 ){  // "string[mark] === '('" would be easier, but this may be expanded
      desc_parts.text = string;
      mark = -1;
      break;
    }
    if( string[mark] === '|' ){
      break;
    }
    mark--;
  }
  if( mark >= 0 ){
    desc_parts.text = string.slice(0,mark);
    var props_string = string.slice(mark+1);
    if( props_string ){
      var props_parts = props_string.trim().split(',');
      props_parts.forEach(function(prop_string){
        var value_parts = prop_string.trim().split(/:|=/g);
        var name_parts = value_parts[0].trim().split('.');
        if( name_parts[0] === 'style'){
          desc_parts.style[name_parts.slice(1).join('.')] = value_parts[1];
        } else {
          desc_parts.props[value_parts[0]] = value_parts[1];
        }
      });
    }

  }

  return desc_parts;
};


/***/ }),

/***/ "./node_modules/markdown_specs/index.js":
/*!**********************************************!*\
  !*** ./node_modules/markdown_specs/index.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var process_table = __webpack_require__(/*! ./process_table */ "./node_modules/markdown_specs/process_table.js");
var process_text = __webpack_require__(/*! ./process_text */ "./node_modules/markdown_specs/process_text.js");
var get_props = __webpack_require__(/*! ./get_props */ "./node_modules/markdown_specs/get_props.js");
/**
 * .
 * @exports
 */
module.exports = function(markdown_text, settings){
  var page_specs = {
    tag: 'div',
    props: {
      class: 'level_0'
    },
    meta: {},
    children: [],
  };
  var lines = markdown_text.split('\n');
  if( ( lines[0].match( /^.*:/ ) || lines[0].slice(0,3) === '---' ) && lines[0][0] !== '#' ){
    var document_parts = markdown_text.split('\n\n');
    var meta_lines = document_parts[0].split('\n');
    lines = lines.slice( meta_lines.length+1 );
    meta_lines.forEach(function(line){
      if( line.trim() !== '' && line.trim() !== '---' ){
        page_specs.meta = page_specs.meta || {};
        var re_clean_params = /<!---|<!--|-->|--->|\||{%|%}/g;
        line = line.replace(re_clean_params,'');
        var line_parts = line.split(':');
        let key = line_parts[0].trim();
        let value = line_parts.slice(1).join(':').trim();
        page_specs.meta[key] = value;
      }
    });
  }
  var re_indent = /^[ ]+/;
  var re_table = /^\|/;
  //var re_special = /^\#+|^\*|^\-{3}/;
  var re_special = /^#+|^ *\*|^ *\+|^ *-|^ *[0-9]+\.|^-{3}/;
  var paragraph_children = [];
  var pre_code_children = [];
  var table_lines = [];
  var h_level = 0;
  var h_parent = [
    page_specs.children
  ];
  var root_ul = false;
  var ul_lastIndent = 0;
  var ul_level = 0;
  var ul_parent = [];
  var root_ol = false;
  var ol_lastIndent = 0;
  var ol_level = 0;
  var ol_parent = [];
  lines.forEach(function(line, i){
    //console.log('--------');
    //console.log(line);
    var indent = 0;
    if( line.match(re_indent) ){
      indent = Math.floor(line.match(re_indent)[0].length/2);
    }
    var special_match = line.trim().match(re_special);
    var table_match = line.match(re_table);
    var line_trimed = line.trim();
    if( line_trimed === '' ){ // blank line
      ul_level = 0;
      ul_lastIndent = 0;
      if( root_ul ){
        h_parent[h_level].push(root_ul);
        root_ul = false;
        ul_parent = [];
      }
      ol_level = 0;
      ol_lastIndent = 0;
      if( root_ol ){
        h_parent[h_level].push(root_ol);
        root_ol = false;
        ol_parent = [];
      }
    }
    if( line_trimed === '' || special_match ){ // blank line or speial character
      // Complete paragraph
      if( paragraph_children[0] !== undefined ){
        h_parent[h_level].push({
          tag: 'p',
          children: paragraph_children,
        });
        paragraph_children = [];
      }
    }
    if( indent < 1 && pre_code_children[0] !== undefined ){
      h_parent[h_level].push({
        tag: 'code',
        children: [
          {
            tag: 'pre',
            text: pre_code_children.join('\n'),
          }
        ]
      });
      pre_code_children = [];
    }
    if( ! table_match && table_lines[0] !== undefined  ){
      h_parent[h_level].push({
        tag: 'table',
        children: process_table(table_lines),
      });
      table_lines = [];
    }
    if( table_match ){
      table_lines.push(line_trimed);
    } else if( special_match ) { // special
      var text = line.trim().slice(special_match[0].length).trim();
      var desc_parts = get_props(text);
      var spec = {};
      if ( Object.keys(desc_parts.props).length !== 0 ){ spec.props = desc_parts.props; }
      if ( Object.keys(desc_parts.style).length !== 0 ){ spec.style = desc_parts.style; }
      //if( line_trimed[0] === '#' ){ // new heading
      if( line[0] === '#' ){ // new heading
        var new_hlevel = line.match(/^#+/)[0].length;
        var section_spec = {
          tag: 'div',
          text: undefined,
          props: {
            class: 'section level_'+new_hlevel
          },
          children: [],
        };
        var level_offset = new_hlevel - h_level;
        if( new_hlevel >= 2 && ! h_parent[h_level+level_offset-1] ) {
          var parent_spec = {
            tag: 'div',
            text: undefined,
            props: {
              class: 'section level_'+new_hlevel-1
            },
            children: [],
          };
          h_parent[h_level+level_offset-1-1].push(parent_spec);
          h_parent[h_level+level_offset-1] = parent_spec.children;
        }
        h_parent[h_level+level_offset-1].push(section_spec);
        h_level = new_hlevel;
        h_parent[h_level] = section_spec.children;
        spec.tag = 'h'+new_hlevel;
        //spec.props = desc_parts.props;
        //spec.style = desc_parts.style;
        spec.children = process_text(desc_parts.text);
        h_parent[h_level].push(spec);
      } else if( line_trimed.match(/^-{3}/) ){ // --- horizontal line
        spec.tag = 'hr';
        // spec.children = process_text(desc_parts.text);
        h_parent[h_level].push(spec);
      } else if ( (line_trimed[0] === '*' && line_trimed[1] !== '*' ) || line_trimed[0] === '+' || line_trimed[0] === '-' ){ // bullet list
        var indent_delta = indent - ul_lastIndent;
        ul_lastIndent = indent;
        if( ! root_ul ){
          root_ul = {
            tag: 'ul',
            props: {
              class: 'list_level_0'
            },
            children: [],
          };
          ul_parent[0] = root_ul.children;
        }
        if( indent_delta > 0 ) {
          ul_level++;
          var ul = {
            tag: 'ul',
            props: {
              class: 'list_level_'+ul_level
            },
            children: [],
          };
          //h_parent[h_level].push(ul);
          ul_parent[ul_level] = ul.children;
          ul_parent[ul_level-1].push( ul );
        } else if( indent_delta < 0 ) {
          ul_level--;
        }
        spec.tag = 'li';
        spec.props = spec.props || {};
        spec.props.class = 'list_level_'+ul_level;
        spec.children = [{
          tag: 'span',
          children: process_text(desc_parts.text),
        }];
        ul_parent[ul_level].push(spec);
      } else if( line_trimed.match(/^[0-9]+\./) ){ // numbered list
        var indent_delta = indent - ol_lastIndent;
        ol_lastIndent = indent;
        if( ! root_ol ){
          root_ol = {
            tag: 'ol',
            props: {
              class: 'list_level_0'
            },
            children: [],
          };
          ol_parent[0] = root_ol.children;
        }
        if( indent_delta > 0 ) {
          ol_level++;
          var ol = {
            tag: 'ol',
            props: {
              class: 'list_level_'+ol_level
            },
            children: [],
          };
          //h_parent[h_level].push(ol);
          ol_parent[ol_level] = ol.children;
          ol_parent[ol_level-1].push( ol );
        } else if( indent_delta < 0 ) {
          ol_level--;
        }
        spec.tag = 'li';
        spec.props = spec.props || {};
        spec.props.class = 'list_level_'+ol_level;
        spec.children = [{
          tag: 'span',
          children: process_text(desc_parts.text),
        }];
        ol_parent[ol_level].push(spec);
      }
    } else if( indent >= 1 ){
      pre_code_children.push(line.slice(2));
    } else if( line_trimed !== '' ) { // paragraph line
      if( line.slice(-1) !== ' ') line += ' ';
      paragraph_children = paragraph_children.concat(process_text(line));
    }
  });
  return page_specs;
};


/***/ }),

/***/ "./node_modules/markdown_specs/process_table.js":
/*!******************************************************!*\
  !*** ./node_modules/markdown_specs/process_table.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var process_text = __webpack_require__(/*! ./process_text */ "./node_modules/markdown_specs/process_text.js");


var re_table_sep = /^\|\s*--|^\|\s*:--/;

function process_table(table_array) {
  var table_header = true;

  var rows = [];

  table_array.forEach(function(line){
    var table_sep_match = line.trim().match(re_table_sep);

    var cells = line.split('|');
    cells.slice(1);
    if(cells[cells.length-1] === ''){
      cells = cells.slice(1,-1);
    } else {
      cells = cells.slice(1);
    }


    if(table_sep_match){
      table_header = false;
    } else {
      rows.push({
        tag: 'tr',
        children: cells.map(function(cell){
          return {
            tag: table_header ? 'th' : 'td',
            children: process_text(cell),
          };
        }),
      });
    }

  });

  return rows;
}

module.exports = process_table;


/***/ }),

/***/ "./node_modules/markdown_specs/process_text.js":
/*!*****************************************************!*\
  !*** ./node_modules/markdown_specs/process_text.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var get_props = __webpack_require__(/*! ./get_props */ "./node_modules/markdown_specs/get_props.js");
var special_chars = __webpack_require__(/*! ./special_chars */ "./node_modules/markdown_specs/special_chars.js");
//ar re_find_links = /\[(.*?)\]\((.+?)\)/g;
//var re_split_line = /\[!\[.*?\]\(.+?\)\]\(.+?\)|(\[.*?\]\(.+?\)|!\[.*?\]\(.+?\))/g;
//var re_split_line = /!\[(.*?)\]\((.+?)\)|(\[.*?\]\(.+?\))/g;
//var re_split_links = /\[(.*?)\]\((.+?)\)/;
var re_divide_links = /\]\(/g;
var re_split_links = /\(|\)|\[|\]/g;
var re_split_images = /!\[(.*?)\]\((.+?)\)/;
var starters = special_chars.starters;
var closers = special_chars.closers;

var convert_special = function(to_convert_string){
  var output_elem = {};
  if ( to_convert_string[0] === '!' ){
    var img_parts = to_convert_string.trim().slice(2,-1).split(re_divide_links);
    var desc_parts = get_props(img_parts[0]);
    var props = desc_parts.props || {};
    props.src = img_parts[1];
    props.alt = desc_parts.text;
    output_elem = {
      tag: 'img',
      props: props
    };
  } else if ( to_convert_string[0] === '['){
    var link_parts = to_convert_string.trim().slice(1,-1).split(re_divide_links);
    var href = link_parts.pop();
    var content = link_parts.join('](');
    var desc_parts = get_props(content);
    var props = desc_parts.props || undefined;
    props.href = href;
    content = desc_parts.text;
    var children = process_text(content);
    output_elem = {
      tag: 'a',
      children: children,
      props: props,
    };
  } else if ( to_convert_string[0] === '*' || to_convert_string[0] === '_' ){
    var tag;
    if ( to_convert_string[1] === '*' || to_convert_string[1] === '_' ){
      tag = 'strong';
    } else {
      tag = 'em';
    }
    var text = to_convert_string.replace(/\*/g, ' ').replace(/_/g, ' ').trim();
    output_elem = {
      tag: tag,
      // text: desc_parts.text,
      //text: text,
      children: [text],
    };
  } else {
    output_elem = to_convert_string;
  }
  return output_elem;
};

var process_text = function process_text(line_string){
  // console.log('-------- ');
  var wait = false;
  var em = false;
  var start = 0;
  var end = 0;
  var starter = false;
  var closer = false;
  var level = 0;
  var output = [];
  if ( ! line_string ){
    return false;
  }
  for( var c = 0; c<line_string.length; c++){
    var section = special_chars.sections[line_string[c]] || false;
    if ( section.preceded_by !== undefined && line_string[c-1] !== undefined && line_string[c-1] !== section.preceded_by ){
      section = false;
    }
    if ( section && ! closer ) { // no closer means we are in a special section
      if ( line_string[c] === '[' && line_string[c-1] === '!' ){ // if trigger is second character of image, then skip
        section = false;
      }
      starter = line_string[c];
      closer = section.closer;
      start = c;
    }
    if ( closer && starter === line_string[c] && ! em ){ // Begin special section of text
      level++;
    }
    if ( closer === '*' || closer === '_' ) {
      if ( line_string[c] === '*' || line_string[c] === '_' ){
        wait = true;
      }
      if (em && ((line_string[c] === '_' && line_string[c + 1] === '_') || (line_string[c] === '*' && line_string[c + 1] === '*'))) {
        wait = false;
      }
      if ( (line_string[c] === '_' && line_string[c + 1] !== '_') || (line_string[c] === '*' && line_string[c + 1] !== '*')) {
        if (em){
          wait = false;
        }
        em = !em;
      }
    }
    if ( ! wait && closer === line_string[c] ){ // End special section of text
      level--;
      if (level === 0){
        if ( line_string.slice(0,start) !== '' ){
          output.push(line_string.slice(end,start));
        }
        output.push(convert_special(line_string.slice(start,c+1)));
        end = c + 1;
        closer = false;
        wait = false;
        // em = false;
      }
    }
  }
  if ( output.length !== 0 ){ // Add closing text to output
    if ( line_string.slice(end) !== '' ){
      output.push(line_string.slice(end));
    }
  } else { // Nothing special was found, outputting input sting
    output.push(line_string);
  }
  return output;
};

module.exports = process_text;


/***/ }),

/***/ "./node_modules/markdown_specs/special_chars.js":
/*!******************************************************!*\
  !*** ./node_modules/markdown_specs/special_chars.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
  starters: [
    '!',
    '[',
    '_',
    '*',
  ],
  closers: [
    ')',
    ')',
    '_',
    '*',
  ],
  sections: {
    '!': {
      closer: ')',
    },
    '[': {
      closer: ')',
    },
    '_': {
      preceded_by: ' ',
      closer: '_',
    },
    '*': {
      preceded_by: ' ',
      closer: '*',
    },
  },
};


/***/ }),

/***/ "./node_modules/normalize.css/normalize.css":
/*!**************************************************!*\
  !*** ./node_modules/normalize.css/normalize.css ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../css-loader/dist/cjs.js!./normalize.css */ "./node_modules/css-loader/dist/cjs.js!./node_modules/normalize.css/normalize.css");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(module.i, content, options);

var exported = content.locals ? content.locals : {};



module.exports = exported;

/***/ }),

/***/ "./node_modules/redux/es/applyMiddleware.js":
/*!**************************************************!*\
  !*** ./node_modules/redux/es/applyMiddleware.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return applyMiddleware; });
/* harmony import */ var _compose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./compose */ "./node_modules/redux/es/compose.js");
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };



/**
 * Creates a store enhancer that applies middleware to the dispatch method
 * of the Redux store. This is handy for a variety of tasks, such as expressing
 * asynchronous actions in a concise manner, or logging every action payload.
 *
 * See `redux-thunk` package as an example of the Redux middleware.
 *
 * Because middleware is potentially asynchronous, this should be the first
 * store enhancer in the composition chain.
 *
 * Note that each middleware will be given the `dispatch` and `getState` functions
 * as named arguments.
 *
 * @param {...Function} middlewares The middleware chain to be applied.
 * @returns {Function} A store enhancer applying the middleware.
 */
function applyMiddleware() {
  for (var _len = arguments.length, middlewares = Array(_len), _key = 0; _key < _len; _key++) {
    middlewares[_key] = arguments[_key];
  }

  return function (createStore) {
    return function (reducer, preloadedState, enhancer) {
      var store = createStore(reducer, preloadedState, enhancer);
      var _dispatch = store.dispatch;
      var chain = [];

      var middlewareAPI = {
        getState: store.getState,
        dispatch: function dispatch(action) {
          return _dispatch(action);
        }
      };
      chain = middlewares.map(function (middleware) {
        return middleware(middlewareAPI);
      });
      _dispatch = _compose__WEBPACK_IMPORTED_MODULE_0__["default"].apply(undefined, chain)(store.dispatch);

      return _extends({}, store, {
        dispatch: _dispatch
      });
    };
  };
}

/***/ }),

/***/ "./node_modules/redux/es/bindActionCreators.js":
/*!*****************************************************!*\
  !*** ./node_modules/redux/es/bindActionCreators.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return bindActionCreators; });
function bindActionCreator(actionCreator, dispatch) {
  return function () {
    return dispatch(actionCreator.apply(undefined, arguments));
  };
}

/**
 * Turns an object whose values are action creators, into an object with the
 * same keys, but with every function wrapped into a `dispatch` call so they
 * may be invoked directly. This is just a convenience method, as you can call
 * `store.dispatch(MyActionCreators.doSomething())` yourself just fine.
 *
 * For convenience, you can also pass a single function as the first argument,
 * and get a function in return.
 *
 * @param {Function|Object} actionCreators An object whose values are action
 * creator functions. One handy way to obtain it is to use ES6 `import * as`
 * syntax. You may also pass a single function.
 *
 * @param {Function} dispatch The `dispatch` function available on your Redux
 * store.
 *
 * @returns {Function|Object} The object mimicking the original object, but with
 * every action creator wrapped into the `dispatch` call. If you passed a
 * function as `actionCreators`, the return value will also be a single
 * function.
 */
function bindActionCreators(actionCreators, dispatch) {
  if (typeof actionCreators === 'function') {
    return bindActionCreator(actionCreators, dispatch);
  }

  if (typeof actionCreators !== 'object' || actionCreators === null) {
    throw new Error('bindActionCreators expected an object or a function, instead received ' + (actionCreators === null ? 'null' : typeof actionCreators) + '. ' + 'Did you write "import ActionCreators from" instead of "import * as ActionCreators from"?');
  }

  var keys = Object.keys(actionCreators);
  var boundActionCreators = {};
  for (var i = 0; i < keys.length; i++) {
    var key = keys[i];
    var actionCreator = actionCreators[key];
    if (typeof actionCreator === 'function') {
      boundActionCreators[key] = bindActionCreator(actionCreator, dispatch);
    }
  }
  return boundActionCreators;
}

/***/ }),

/***/ "./node_modules/redux/es/combineReducers.js":
/*!**************************************************!*\
  !*** ./node_modules/redux/es/combineReducers.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return combineReducers; });
/* harmony import */ var _createStore__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./createStore */ "./node_modules/redux/es/createStore.js");
/* harmony import */ var lodash_es_isPlainObject__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash-es/isPlainObject */ "./node_modules/lodash-es/isPlainObject.js");
/* harmony import */ var _utils_warning__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./utils/warning */ "./node_modules/redux/es/utils/warning.js");




function getUndefinedStateErrorMessage(key, action) {
  var actionType = action && action.type;
  var actionName = actionType && '"' + actionType.toString() + '"' || 'an action';

  return 'Given action ' + actionName + ', reducer "' + key + '" returned undefined. ' + 'To ignore an action, you must explicitly return the previous state. ' + 'If you want this reducer to hold no value, you can return null instead of undefined.';
}

function getUnexpectedStateShapeWarningMessage(inputState, reducers, action, unexpectedKeyCache) {
  var reducerKeys = Object.keys(reducers);
  var argumentName = action && action.type === _createStore__WEBPACK_IMPORTED_MODULE_0__["ActionTypes"].INIT ? 'preloadedState argument passed to createStore' : 'previous state received by the reducer';

  if (reducerKeys.length === 0) {
    return 'Store does not have a valid reducer. Make sure the argument passed ' + 'to combineReducers is an object whose values are reducers.';
  }

  if (!Object(lodash_es_isPlainObject__WEBPACK_IMPORTED_MODULE_1__["default"])(inputState)) {
    return 'The ' + argumentName + ' has unexpected type of "' + {}.toString.call(inputState).match(/\s([a-z|A-Z]+)/)[1] + '". Expected argument to be an object with the following ' + ('keys: "' + reducerKeys.join('", "') + '"');
  }

  var unexpectedKeys = Object.keys(inputState).filter(function (key) {
    return !reducers.hasOwnProperty(key) && !unexpectedKeyCache[key];
  });

  unexpectedKeys.forEach(function (key) {
    unexpectedKeyCache[key] = true;
  });

  if (unexpectedKeys.length > 0) {
    return 'Unexpected ' + (unexpectedKeys.length > 1 ? 'keys' : 'key') + ' ' + ('"' + unexpectedKeys.join('", "') + '" found in ' + argumentName + '. ') + 'Expected to find one of the known reducer keys instead: ' + ('"' + reducerKeys.join('", "') + '". Unexpected keys will be ignored.');
  }
}

function assertReducerShape(reducers) {
  Object.keys(reducers).forEach(function (key) {
    var reducer = reducers[key];
    var initialState = reducer(undefined, { type: _createStore__WEBPACK_IMPORTED_MODULE_0__["ActionTypes"].INIT });

    if (typeof initialState === 'undefined') {
      throw new Error('Reducer "' + key + '" returned undefined during initialization. ' + 'If the state passed to the reducer is undefined, you must ' + 'explicitly return the initial state. The initial state may ' + 'not be undefined. If you don\'t want to set a value for this reducer, ' + 'you can use null instead of undefined.');
    }

    var type = '@@redux/PROBE_UNKNOWN_ACTION_' + Math.random().toString(36).substring(7).split('').join('.');
    if (typeof reducer(undefined, { type: type }) === 'undefined') {
      throw new Error('Reducer "' + key + '" returned undefined when probed with a random type. ' + ('Don\'t try to handle ' + _createStore__WEBPACK_IMPORTED_MODULE_0__["ActionTypes"].INIT + ' or other actions in "redux/*" ') + 'namespace. They are considered private. Instead, you must return the ' + 'current state for any unknown actions, unless it is undefined, ' + 'in which case you must return the initial state, regardless of the ' + 'action type. The initial state may not be undefined, but can be null.');
    }
  });
}

/**
 * Turns an object whose values are different reducer functions, into a single
 * reducer function. It will call every child reducer, and gather their results
 * into a single state object, whose keys correspond to the keys of the passed
 * reducer functions.
 *
 * @param {Object} reducers An object whose values correspond to different
 * reducer functions that need to be combined into one. One handy way to obtain
 * it is to use ES6 `import * as reducers` syntax. The reducers may never return
 * undefined for any action. Instead, they should return their initial state
 * if the state passed to them was undefined, and the current state for any
 * unrecognized action.
 *
 * @returns {Function} A reducer function that invokes every reducer inside the
 * passed object, and builds a state object with the same shape.
 */
function combineReducers(reducers) {
  var reducerKeys = Object.keys(reducers);
  var finalReducers = {};
  for (var i = 0; i < reducerKeys.length; i++) {
    var key = reducerKeys[i];

    if (true) {
      if (typeof reducers[key] === 'undefined') {
        Object(_utils_warning__WEBPACK_IMPORTED_MODULE_2__["default"])('No reducer provided for key "' + key + '"');
      }
    }

    if (typeof reducers[key] === 'function') {
      finalReducers[key] = reducers[key];
    }
  }
  var finalReducerKeys = Object.keys(finalReducers);

  var unexpectedKeyCache = void 0;
  if (true) {
    unexpectedKeyCache = {};
  }

  var shapeAssertionError = void 0;
  try {
    assertReducerShape(finalReducers);
  } catch (e) {
    shapeAssertionError = e;
  }

  return function combination() {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    var action = arguments[1];

    if (shapeAssertionError) {
      throw shapeAssertionError;
    }

    if (true) {
      var warningMessage = getUnexpectedStateShapeWarningMessage(state, finalReducers, action, unexpectedKeyCache);
      if (warningMessage) {
        Object(_utils_warning__WEBPACK_IMPORTED_MODULE_2__["default"])(warningMessage);
      }
    }

    var hasChanged = false;
    var nextState = {};
    for (var _i = 0; _i < finalReducerKeys.length; _i++) {
      var _key = finalReducerKeys[_i];
      var reducer = finalReducers[_key];
      var previousStateForKey = state[_key];
      var nextStateForKey = reducer(previousStateForKey, action);
      if (typeof nextStateForKey === 'undefined') {
        var errorMessage = getUndefinedStateErrorMessage(_key, action);
        throw new Error(errorMessage);
      }
      nextState[_key] = nextStateForKey;
      hasChanged = hasChanged || nextStateForKey !== previousStateForKey;
    }
    return hasChanged ? nextState : state;
  };
}

/***/ }),

/***/ "./node_modules/redux/es/compose.js":
/*!******************************************!*\
  !*** ./node_modules/redux/es/compose.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return compose; });
/**
 * Composes single-argument functions from right to left. The rightmost
 * function can take multiple arguments as it provides the signature for
 * the resulting composite function.
 *
 * @param {...Function} funcs The functions to compose.
 * @returns {Function} A function obtained by composing the argument functions
 * from right to left. For example, compose(f, g, h) is identical to doing
 * (...args) => f(g(h(...args))).
 */

function compose() {
  for (var _len = arguments.length, funcs = Array(_len), _key = 0; _key < _len; _key++) {
    funcs[_key] = arguments[_key];
  }

  if (funcs.length === 0) {
    return function (arg) {
      return arg;
    };
  }

  if (funcs.length === 1) {
    return funcs[0];
  }

  return funcs.reduce(function (a, b) {
    return function () {
      return a(b.apply(undefined, arguments));
    };
  });
}

/***/ }),

/***/ "./node_modules/redux/es/createStore.js":
/*!**********************************************!*\
  !*** ./node_modules/redux/es/createStore.js ***!
  \**********************************************/
/*! exports provided: ActionTypes, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionTypes", function() { return ActionTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return createStore; });
/* harmony import */ var lodash_es_isPlainObject__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lodash-es/isPlainObject */ "./node_modules/lodash-es/isPlainObject.js");
/* harmony import */ var symbol_observable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! symbol-observable */ "./node_modules/symbol-observable/es/index.js");



/**
 * These are private action types reserved by Redux.
 * For any unknown actions, you must return the current state.
 * If the current state is undefined, you must return the initial state.
 * Do not reference these action types directly in your code.
 */
var ActionTypes = {
  INIT: '@@redux/INIT'

  /**
   * Creates a Redux store that holds the state tree.
   * The only way to change the data in the store is to call `dispatch()` on it.
   *
   * There should only be a single store in your app. To specify how different
   * parts of the state tree respond to actions, you may combine several reducers
   * into a single reducer function by using `combineReducers`.
   *
   * @param {Function} reducer A function that returns the next state tree, given
   * the current state tree and the action to handle.
   *
   * @param {any} [preloadedState] The initial state. You may optionally specify it
   * to hydrate the state from the server in universal apps, or to restore a
   * previously serialized user session.
   * If you use `combineReducers` to produce the root reducer function, this must be
   * an object with the same shape as `combineReducers` keys.
   *
   * @param {Function} [enhancer] The store enhancer. You may optionally specify it
   * to enhance the store with third-party capabilities such as middleware,
   * time travel, persistence, etc. The only store enhancer that ships with Redux
   * is `applyMiddleware()`.
   *
   * @returns {Store} A Redux store that lets you read the state, dispatch actions
   * and subscribe to changes.
   */
};function createStore(reducer, preloadedState, enhancer) {
  var _ref2;

  if (typeof preloadedState === 'function' && typeof enhancer === 'undefined') {
    enhancer = preloadedState;
    preloadedState = undefined;
  }

  if (typeof enhancer !== 'undefined') {
    if (typeof enhancer !== 'function') {
      throw new Error('Expected the enhancer to be a function.');
    }

    return enhancer(createStore)(reducer, preloadedState);
  }

  if (typeof reducer !== 'function') {
    throw new Error('Expected the reducer to be a function.');
  }

  var currentReducer = reducer;
  var currentState = preloadedState;
  var currentListeners = [];
  var nextListeners = currentListeners;
  var isDispatching = false;

  function ensureCanMutateNextListeners() {
    if (nextListeners === currentListeners) {
      nextListeners = currentListeners.slice();
    }
  }

  /**
   * Reads the state tree managed by the store.
   *
   * @returns {any} The current state tree of your application.
   */
  function getState() {
    return currentState;
  }

  /**
   * Adds a change listener. It will be called any time an action is dispatched,
   * and some part of the state tree may potentially have changed. You may then
   * call `getState()` to read the current state tree inside the callback.
   *
   * You may call `dispatch()` from a change listener, with the following
   * caveats:
   *
   * 1. The subscriptions are snapshotted just before every `dispatch()` call.
   * If you subscribe or unsubscribe while the listeners are being invoked, this
   * will not have any effect on the `dispatch()` that is currently in progress.
   * However, the next `dispatch()` call, whether nested or not, will use a more
   * recent snapshot of the subscription list.
   *
   * 2. The listener should not expect to see all state changes, as the state
   * might have been updated multiple times during a nested `dispatch()` before
   * the listener is called. It is, however, guaranteed that all subscribers
   * registered before the `dispatch()` started will be called with the latest
   * state by the time it exits.
   *
   * @param {Function} listener A callback to be invoked on every dispatch.
   * @returns {Function} A function to remove this change listener.
   */
  function subscribe(listener) {
    if (typeof listener !== 'function') {
      throw new Error('Expected listener to be a function.');
    }

    var isSubscribed = true;

    ensureCanMutateNextListeners();
    nextListeners.push(listener);

    return function unsubscribe() {
      if (!isSubscribed) {
        return;
      }

      isSubscribed = false;

      ensureCanMutateNextListeners();
      var index = nextListeners.indexOf(listener);
      nextListeners.splice(index, 1);
    };
  }

  /**
   * Dispatches an action. It is the only way to trigger a state change.
   *
   * The `reducer` function, used to create the store, will be called with the
   * current state tree and the given `action`. Its return value will
   * be considered the **next** state of the tree, and the change listeners
   * will be notified.
   *
   * The base implementation only supports plain object actions. If you want to
   * dispatch a Promise, an Observable, a thunk, or something else, you need to
   * wrap your store creating function into the corresponding middleware. For
   * example, see the documentation for the `redux-thunk` package. Even the
   * middleware will eventually dispatch plain object actions using this method.
   *
   * @param {Object} action A plain object representing “what changed”. It is
   * a good idea to keep actions serializable so you can record and replay user
   * sessions, or use the time travelling `redux-devtools`. An action must have
   * a `type` property which may not be `undefined`. It is a good idea to use
   * string constants for action types.
   *
   * @returns {Object} For convenience, the same action object you dispatched.
   *
   * Note that, if you use a custom middleware, it may wrap `dispatch()` to
   * return something else (for example, a Promise you can await).
   */
  function dispatch(action) {
    if (!Object(lodash_es_isPlainObject__WEBPACK_IMPORTED_MODULE_0__["default"])(action)) {
      throw new Error('Actions must be plain objects. ' + 'Use custom middleware for async actions.');
    }

    if (typeof action.type === 'undefined') {
      throw new Error('Actions may not have an undefined "type" property. ' + 'Have you misspelled a constant?');
    }

    if (isDispatching) {
      throw new Error('Reducers may not dispatch actions.');
    }

    try {
      isDispatching = true;
      currentState = currentReducer(currentState, action);
    } finally {
      isDispatching = false;
    }

    var listeners = currentListeners = nextListeners;
    for (var i = 0; i < listeners.length; i++) {
      var listener = listeners[i];
      listener();
    }

    return action;
  }

  /**
   * Replaces the reducer currently used by the store to calculate the state.
   *
   * You might need this if your app implements code splitting and you want to
   * load some of the reducers dynamically. You might also need this if you
   * implement a hot reloading mechanism for Redux.
   *
   * @param {Function} nextReducer The reducer for the store to use instead.
   * @returns {void}
   */
  function replaceReducer(nextReducer) {
    if (typeof nextReducer !== 'function') {
      throw new Error('Expected the nextReducer to be a function.');
    }

    currentReducer = nextReducer;
    dispatch({ type: ActionTypes.INIT });
  }

  /**
   * Interoperability point for observable/reactive libraries.
   * @returns {observable} A minimal observable of state changes.
   * For more information, see the observable proposal:
   * https://github.com/tc39/proposal-observable
   */
  function observable() {
    var _ref;

    var outerSubscribe = subscribe;
    return _ref = {
      /**
       * The minimal observable subscription method.
       * @param {Object} observer Any object that can be used as an observer.
       * The observer object should have a `next` method.
       * @returns {subscription} An object with an `unsubscribe` method that can
       * be used to unsubscribe the observable from the store, and prevent further
       * emission of values from the observable.
       */
      subscribe: function subscribe(observer) {
        if (typeof observer !== 'object') {
          throw new TypeError('Expected the observer to be an object.');
        }

        function observeState() {
          if (observer.next) {
            observer.next(getState());
          }
        }

        observeState();
        var unsubscribe = outerSubscribe(observeState);
        return { unsubscribe: unsubscribe };
      }
    }, _ref[symbol_observable__WEBPACK_IMPORTED_MODULE_1__["default"]] = function () {
      return this;
    }, _ref;
  }

  // When a store is created, an "INIT" action is dispatched so that every
  // reducer returns their initial state. This effectively populates
  // the initial state tree.
  dispatch({ type: ActionTypes.INIT });

  return _ref2 = {
    dispatch: dispatch,
    subscribe: subscribe,
    getState: getState,
    replaceReducer: replaceReducer
  }, _ref2[symbol_observable__WEBPACK_IMPORTED_MODULE_1__["default"]] = observable, _ref2;
}

/***/ }),

/***/ "./node_modules/redux/es/index.js":
/*!****************************************!*\
  !*** ./node_modules/redux/es/index.js ***!
  \****************************************/
/*! exports provided: createStore, combineReducers, bindActionCreators, applyMiddleware, compose */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _createStore__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./createStore */ "./node_modules/redux/es/createStore.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "createStore", function() { return _createStore__WEBPACK_IMPORTED_MODULE_0__["default"]; });

/* harmony import */ var _combineReducers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./combineReducers */ "./node_modules/redux/es/combineReducers.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "combineReducers", function() { return _combineReducers__WEBPACK_IMPORTED_MODULE_1__["default"]; });

/* harmony import */ var _bindActionCreators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./bindActionCreators */ "./node_modules/redux/es/bindActionCreators.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "bindActionCreators", function() { return _bindActionCreators__WEBPACK_IMPORTED_MODULE_2__["default"]; });

/* harmony import */ var _applyMiddleware__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./applyMiddleware */ "./node_modules/redux/es/applyMiddleware.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "applyMiddleware", function() { return _applyMiddleware__WEBPACK_IMPORTED_MODULE_3__["default"]; });

/* harmony import */ var _compose__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./compose */ "./node_modules/redux/es/compose.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "compose", function() { return _compose__WEBPACK_IMPORTED_MODULE_4__["default"]; });

/* harmony import */ var _utils_warning__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./utils/warning */ "./node_modules/redux/es/utils/warning.js");







/*
* This is a dummy function to check if the function name has been altered by minification.
* If the function has been minified and NODE_ENV !== 'production', warn the user.
*/
function isCrushed() {}

if ( true && typeof isCrushed.name === 'string' && isCrushed.name !== 'isCrushed') {
  Object(_utils_warning__WEBPACK_IMPORTED_MODULE_5__["default"])('You are currently using minified code outside of NODE_ENV === \'production\'. ' + 'This means that you are running a slower development build of Redux. ' + 'You can use loose-envify (https://github.com/zertosh/loose-envify) for browserify ' + 'or DefinePlugin for webpack (http://stackoverflow.com/questions/30030031) ' + 'to ensure you have the correct code for your production build.');
}



/***/ }),

/***/ "./node_modules/redux/es/utils/warning.js":
/*!************************************************!*\
  !*** ./node_modules/redux/es/utils/warning.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return warning; });
/**
 * Prints a warning in the console if it exists.
 *
 * @param {String} message The warning message.
 * @returns {void}
 */
function warning(message) {
  /* eslint-disable no-console */
  if (typeof console !== 'undefined' && typeof console.error === 'function') {
    console.error(message);
  }
  /* eslint-enable no-console */
  try {
    // This error was thrown as a convenience so that if you enable
    // "break on all exceptions" in your console,
    // it would pause the execution at this line.
    throw new Error(message);
    /* eslint-disable no-empty */
  } catch (e) {}
  /* eslint-enable no-empty */
}

/***/ }),

/***/ "./node_modules/specdom/DOM_update.js":
/*!********************************************!*\
  !*** ./node_modules/specdom/DOM_update.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var mkDOM = __webpack_require__(/*! ./mkDOM.js */ "./node_modules/specdom/mkDOM.js");


var configChanged = function(old_specs, new_specs){
  var old_specs_clone = JSON.parse(JSON.stringify(old_specs));
  old_specs_clone.children = undefined;
  var new_specs_clone = JSON.parse(JSON.stringify(new_specs));
  new_specs_clone.children = undefined;
  return JSON.stringify(old_specs_clone) !== JSON.stringify(new_specs_clone);
};

var default_level = 0;

function DOM_update(sdom,new_specs){
  // console.log('_______');
  var old_specs = ( sdom && sdom.specs ) || {};
  // sdom.specs = new_specs;

  if( ! new_specs ) {
    // console.log('|>');
    // console.log('|>');
    if( sdom && sdom.elem ){
      if(sdom.elem.parentNode){
        sdom.elem.parentNode.removeChild(sdom.elem);
      } else {
        console.log('parrentNode missing:',sdom,new_specs);
      }
    }
    sdom = undefined;
  } else if( configChanged(old_specs, new_specs) ){ // CHANGE
    console.log('A/B');
    var existing_elem = sdom.elem;
    sdom = sdom.mkDOM(new_specs,sdom);
    if(existing_elem.parentNode){
      existing_elem.parentNode.replaceChild(sdom.elem,existing_elem);
    } else {
      console.log('parrentNode missing:',existing_elem);
    }
    // parent.append(sdom);
  } else if( JSON.stringify(old_specs.children) !== JSON.stringify(new_specs.children) ){ // Children changed
    var oldLength = ( sdom && sdom.children && sdom.children.length ) || 0;
    var newLength = ( new_specs && new_specs.children && new_specs.children.length ) || 0;
    var l = Math.max(oldLength,newLength);
    // console.log(oldLength,newLength,l);
    // console.log( old_specs.children.map(function(child){return child;}) );
    // console.log( new_specs.children.map(function(child){return child;}) );
    var new_children = [];
    // sdom.clear()
    for(var i = 0; i < l; i++) {
      var old_child_sdom = sdom && sdom.children && sdom.children[i];
      var new_child_specs = new_specs && new_specs.children && new_specs.children[i];
      // console.log(i,new_child_specs);
      if( ! old_child_sdom ){
        // sdom.append(sdom.mkDOM(new_child_specs));
        new_children.push( sdom.mkDOM(new_child_specs) );
      } else {
        // sdom.children[i] = DOM_update(old_child_sdom, new_child_specs);
        new_children.push( DOM_update(old_child_sdom, new_child_specs) );
      }
    }
    sdom.clear().append(new_children);
    //*/
  } else {
    // console.log('_');
  }

  return sdom;
}

module.exports = DOM_update;


/***/ }),

/***/ "./node_modules/specdom/index.js":
/*!***************************************!*\
  !*** ./node_modules/specdom/index.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/**
* A simple DOM manipulation tool.
 * @fileOverview Constructor for SimpleDOM function.
 * @author Keith Showalter {@link https://github.com/kshowalter}
 * @version 0.2.0
 */


/**
 * @module
 */
var mkDOM = __webpack_require__(/*! ./mkDOM.js */ "./node_modules/specdom/mkDOM.js");


/**
 * Wraps an HTMLElement with a jquery like function
 * @param  {string} input
 * @return {SimpleDOM} Wrapped HTMLElement
 */
var $ = function $(input, specs){
  if ( typeof input === 'undefined' ) {
    console.log('input needed, using "body"');
    // return false;
    input = 'body';
  }

  if ( input.constructor === Object && input.elem ){
    return input;
  }

  if ( input.constructor === Object && ( input.tag || input._ ) && specs === undefined ){
    specs = input;
    input = false;
  }

  if ( input ){
    var target_sdom;
    var element;
    var elements;
    if ( input.nodeName !== undefined || input.constructor.prototype === HTMLElement || input instanceof SVGElement ) {
      element = input;
      target_sdom = mkDOM(element);
    } else if ( input.substr && input.substr(0,1) === '#' ) {
      element = document.getElementById(input.substr(1));
      if ( element ){
        target_sdom = mkDOM(element);
      } else {
        target_sdom = false;
        console.error('Element not found for id:',input);
      }
    } else if ( input.substr && input.substr(0,1) === '.' ) {
      elements = [].slice.call(document.getElementsByClassName(input.substr(1)));
      target_sdom = elements.map(function(element){ return (mkDOM(element)); });
    } else if ( input === 'body' ) {
      elements = document.body;
      target_sdom = mkDOM(elements);
    } else {
      specs = specs || {};
      specs.tag = input;
    }
  }

  if ( specs ){
    var sdom = mkDOM(specs);
    if (target_sdom){
      target_sdom.append(sdom);
    }
    return sdom;
  } else {
    return target_sdom;
  }

};

/**
 * Constructor for SimpleDOM function.
 * @exports $
 */
module.exports = $;


/***/ }),

/***/ "./node_modules/specdom/mkDOM.js":
/*!***************************************!*\
  !*** ./node_modules/specdom/mkDOM.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var wrapper_prototype = __webpack_require__(/*! ./wrapper_prototype.js */ "./node_modules/specdom/wrapper_prototype.js");

var cspec_replace = {
  '_': 'tag',
  '_c': 'children',
  '_t': 'text',
  '_m': 'meta',
  '_p': 'props',
};

function decompact(spec){
  Object.keys(spec).forEach( (keyname)=>{
    if ( cspec_replace[keyname] ){
      spec[cspec_replace[keyname]] = spec[keyname];
      delete spec[keyname];
    }
  });
  //*
  let tag = spec.tag;
  delete spec.tag;
  let text = spec.text;
  delete spec.text;
  let children = spec.children;
  delete spec.children;
  let meta = spec.meta;
  delete spec.meta;
  let elem = spec.elem;
  delete spec.elem;
  let style = spec.style;
  delete spec.style;
  let props = Object.assign({}, spec.props);
  delete spec.props;
  props = Object.assign(props, spec);
  if ( Object.entries(props).length === 0 ) props = undefined;
  for (const prop of Object.getOwnPropertyNames(spec)) {
    delete spec[prop];
  }
  spec = Object.assign(spec, {
    tag,
    text,
    props,
    children,
    meta,
    elem,
    style,
  });
  if ( spec.props === undefined || Object.entries(spec.props).length === 0 ) delete spec.props;
  if ( spec.children === undefined ) delete spec.children;
  if ( spec.meta === undefined || Object.entries(spec.meta).length === 0 ) delete spec.meta;
  return spec;
}

/**
* mkDOM - Makes DOM Element from a ConfigDOM specs object
* @function
* @param  {object} specs ConfigDOM specs object
* @return {element} DOM Element
*/
var mkDOM = function mkDOM(specs, sdom){
  if ( !specs ){
    specs = {};
  }
  ///////////////////////////////
  // Prep specs
  if ( specs.constructor === Number ){
    specs = specs.toString();
  }
  if ( specs.constructor === String ){
    //sdom = document.createTextNode(specs);
    specs = {
      tag: 'textNode',
      text: specs
    };
  }
  if ( specs.nodeName !== undefined || specs.constructor.prototype === HTMLElement || specs instanceof SVGElement ) { // NODE ELEMENT
    specs = {
      tag: 'elem',
      elem: specs
    };
  }

  specs = decompact(specs);
  if ( specs.children && specs.children.constructor !== Array ) {
    specs.children = [specs.children];
  }
  specs.props = specs.props || {};
  specs.meta = specs.meta || {};
  ///////////////////////////
  // make element
  sdom = sdom || Object.create(wrapper_prototype);
  sdom.mkDOM = mkDOM;
  sdom.specs = specs;
  sdom.children = [];
  var namespaceURI = specs.props.namespaceURI || false;
  namespaceURI = specs.namespaceURI || namespaceURI;
  namespaceURI = specs.meta.namespaceURI || namespaceURI;
  if ( specs.tag === 'elem' ) { // NODE ELEMENT
    sdom.elem = specs.elem;
    sdom.specs = {};
  } else { // CONFIG
    if ( namespaceURI ) {
      sdom.elem = document.createElementNS(namespaceURI, specs.tag);
    } else if ( specs.textNode ) {
      sdom.elem = document.createTextNode(specs.text);
    } else if ( specs.tag ){
      sdom.elem = document.createElement(specs.tag);
    } else {
      console.log('Elem type not found.', specs);
      return false;
    }
    if ( specs.props ){
      for ( let name in specs.props ){
        if ( name === 'value'){
          if ( specs.props[name] !== undefined ){
            sdom.value(specs.props[name]);
          }
        } else {
          sdom.attr(name, specs.props[name]);
        }
      }
    }
    if ( specs.style ){
      for ( let name in specs.style ){
        sdom.style(name, specs.style[name]);
      }
    }
    if ( specs.text ){
      sdom.text( specs.text );
    }
  }
  if ( specs.children && specs.children.length ){
    for (var i = 0; i < specs.children.length; i++) {
      var newChild = specs.children[i];
      var child_sdom = mkDOM(newChild);
      if ( child_sdom && child_sdom.constructor === Object ){
        // Add shild sDOM to this sDOM's child array
        //sdom.children.push(child_sdom);
        // Append child element to this element, with built in function.
        sdom.append(child_sdom);
      }
    }
  }
  if ( specs.props && specs.props.value !== undefined){
    sdom.value(specs.props.value);
  }
  return sdom;
};


module.exports = mkDOM;


/***/ }),

/***/ "./node_modules/specdom/wrapper_prototype.js":
/*!***************************************************!*\
  !*** ./node_modules/specdom/wrapper_prototype.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var DOM_update = __webpack_require__(/*! ./DOM_update.js */ "./node_modules/specdom/DOM_update.js");

/**
 * Prototype object for the element wrapper.
 */
var wrapper_prototype = {
  text: function(string){
    if(this.elem){
      this.elem.textContent = string;
    } else {
      console.log('elem missing');
    }
    return this;
  },
  append: function(input){
    if( input.constructor === Array ){
      for( var i=0; i<input.length; i++ ){
        this.append(input[i]);
      }
    } else {
      var child_sdom;
      if( input.elem ){
        child_sdom = input;
        this.elem.appendChild(child_sdom.elem);
        this.children.push(child_sdom);
      } else {
        child_sdom = this.mkDOM(input);
        this.elem.appendChild(child_sdom.elem);
        this.children.push(child_sdom);
      }

      /*
      if( input.constructor === String || input.constructor === Number){
        input = document.createTextNode(input);
        this.elem.appendChild(input);
      } else if( input.constructor === Node ){
        this.elem.appendChild(input);
      } else if( input.tag ){
        var sub_sdom = this.mkDOM(input);
        this.elem.appendChild(sub_sdom.elem);
      } else if( input.elem ){
        this.elem.appendChild(input.elem);
      }
      this.children.push(input);
      */
    }
    return this;
  },
  appendTo: function(parentElement){
    if( parentElement.constructor === Node ){
      parentElement.appendChild(this.elem);
    } else {
      parentElement.append(this);
    }
    return this;
  },
  attr: function(name, value){
    var attributeName;
    if( name === 'class'){
      attributeName = 'class';
    } else {
      attributeName = name;
    }
    if( typeof value === 'function' ){
      this.elem[attributeName] = value;
    } else if( value === undefined ){
      return this.elem.getAttribute(attributeName);
    } else if( value === null ){
      this.elem.removeAttribute(attributeName);
    } else if( attributeName === 'innerHTML' ){
      this.elem[attributeName] = value;
    } else {
      this.elem.setAttribute(attributeName, value);
    }
    //console.log(attributeName, value);
    return this;
  },
  unwrap: function(){
    return this.elem;
  },
  clear: function(){
    //this.elem.innerHTML = '';
    console.log( this.elem );
    while (this.elem.firstChild) {
      this.elem.removeChild(this.elem.firstChild);
    }
    this.children = [];
    return this;
  },
  load: function(new_specs){
    console.log(new_specs);
    DOM_update(this,new_specs);
    console.log(this);
  },
  parent: function(){
    return this.mkDOM(this.elem.parentNode);
  },
  set: function(new_element){
    var parent = this.parent(new_element);
    parent.clear().append(new_element);
    return this;
  },
  set_value: function(new_value){
    if( new_value !== undefined){
      this.value(new_value);
    }
    return this;
  },
  style: function(name, value){
    if( value === undefined ){
      return this.elem.style[name];
    }
    this.elem.style[name] = value;
    return this;
  },
  value: function(new_value){
    if( new_value === undefined){
      return this.elem.value;
    } else {
      if( this.elem && this.elem.value !== undefined ){
        this.elem.value = new_value;
        return this;
      } else {
        return false;
      }
    }

  },
};


/**
 * @constructor Wraps an HTMLElement with a jquery like function.
 * @param  {HTMLElement} element
 * @return {function} HTMLElement wrapped by SimpleDOM
 */
// function Wrap(element){
//   var W = Object.create(wrapper_prototype);
//   W.elem = element;
//   return W;
// }


/**
 * @exports wrapper_prototype
 */
module.exports = wrapper_prototype;


/***/ }),

/***/ "./node_modules/specdom_tools/index.js":
/*!*********************************************!*\
  !*** ./node_modules/specdom_tools/index.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports.specs_to_html = __webpack_require__(/*! ./lib/specs_to_html */ "./node_modules/specdom_tools/lib/specs_to_html.js");
module.exports.compact = __webpack_require__(/*! ./lib/compact */ "./node_modules/specdom_tools/lib/compact.js");
module.exports.expand = __webpack_require__(/*! ./lib/expand */ "./node_modules/specdom_tools/lib/expand.js");
module.exports.markdown_to_specdom = __webpack_require__(/*! ./lib/markdown_to_specdom */ "./node_modules/specdom_tools/lib/markdown_to_specdom.js");
module.exports.fix_link_path = __webpack_require__(/*! ./lib/fix_link_path */ "./node_modules/specdom_tools/lib/fix_link_path.js");
module.exports.markdown_specs = __webpack_require__(/*! markdown_specs */ "./node_modules/markdown_specs/index.js");
module.exports.variate = __webpack_require__(/*! ./lib/variate */ "./node_modules/specdom_tools/lib/variate.js");



/***/ }),

/***/ "./node_modules/specdom_tools/lib/compact.js":
/*!***************************************************!*\
  !*** ./node_modules/specdom_tools/lib/compact.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var cspec_replace = {
  'tag': '_',
  'children': '_c',
  'text': '_t',
  'meta': '_m',
  'props': '_p',
  'style': '_s',
};

module.exports = function compact(spec){
  Object.keys(spec).forEach( key =>{
    if ( cspec_replace[key] ){
      spec[cspec_replace[key]] = spec[key];
      delete spec[key];
    }
  });
  if ( spec._c && spec._c.constructor === Array ){
    let children = spec._c;
    spec._c = [];
    children.forEach( subspec => {
      spec._c.push( compact(subspec) );
    });
  }
  return spec;
};


/***/ }),

/***/ "./node_modules/specdom_tools/lib/expand.js":
/*!**************************************************!*\
  !*** ./node_modules/specdom_tools/lib/expand.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var cspec_replace = {
  '_': 'tag',
  '_c': 'children',
  '_t': 'text',
  '_m': 'meta',
  '_p': 'props',
  '_s': 'style',
};

module.exports = function expand(spec){
  Object.keys(spec).forEach( key =>{
    if ( cspec_replace[key] ){
      spec[cspec_replace[key]] = spec[key];
      delete spec[key];
    }
  });
  if ( spec.children && spec.children.constructor === Array ){
    let children = spec.children;
    spec.children = [];
    children.forEach( subspec => {
      spec.children.push( expand(subspec) );
    });
  }
  return spec;
};


/***/ }),

/***/ "./node_modules/specdom_tools/lib/fix_link_path.js":
/*!*********************************************************!*\
  !*** ./node_modules/specdom_tools/lib/fix_link_path.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var default_config = {
  path: './#/',
  root_path: './#/',
  rules: [
    {
      // relative link
      tag: 'a',
      link_type: 'href',
      not: /^https*:\/\/|^\//,
      replace: /^\.\/|\.md/g,
      prepend_path: true,
    },
    {
      // absolute link
      tag: 'a',
      link_type: 'href',
      find: /^\//,
      not: /^\/#\//,
      replace: /\.md/g,
    },
    {
      tag: 'img',
      link_type: 'src',
      prepend_string: '/assets/',
    },
  ]
};
function fix_link_path(specs,config){
  config = Object.assign({}, default_config, config);
  let path = config.path;
  config.rules.forEach( rule => {
    let link_string = specs.props && specs.props[rule.link_type];
    if ( link_string && specs.tag === rule.tag ){
      let should_prepend = true;
      if ( rule.find !== undefined ) {
        should_prepend = link_string.match( rule.find ) !== null;
      }
      if ( rule.not !== undefined ) {
        should_prepend = should_prepend && link_string.match( rule.not ) === null;
      }
      if ( should_prepend ){
        if ( rule.replace !== undefined ){
          link_string = link_string.replace( rule.replace, '' );
        }
        if ( rule.prepend_string !== undefined ){
          link_string = rule.prepend_string + link_string;
        }
        if ( rule.prepend_path === true ){
          link_string = path + link_string;
        }
        specs.props[rule.link_type] = link_string;
      }
    }
  });
  if ( specs.children ){
    specs.children.forEach( subspec => fix_link_path(subspec,config) );
  }
  return specs;
}
module.exports = fix_link_path;


/***/ }),

/***/ "./node_modules/specdom_tools/lib/markdown_to_specdom.js":
/*!***************************************************************!*\
  !*** ./node_modules/specdom_tools/lib/markdown_to_specdom.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var f = __webpack_require__(/*! functions */ "./node_modules/functions/index.js");
var markdown_specs = __webpack_require__(/*! markdown_specs */ "./node_modules/markdown_specs/index.js");

var convertion_functions_default = {
  'md': markdown_specs,
};

function markdown_to_specdom(markdown_strings, convertion_function){
  convertion_function = convertion_function || convertion_functions_default.md;
  var pages = {};
  for ( let page_id in markdown_strings ){
    let page_spec = convertion_function(markdown_strings[page_id]);
    let page_name = page_id.split('/').slice(-1)[0];
    var title = f.pretty_name(page_name);
    page_spec.props = page_spec.props || {};
    page_spec.props['id'] = 'page';
    page_spec.meta = page_spec.meta || {};
    page_spec.meta['page_id'] = page_id;
    page_spec.meta['title'] = title;
    page_spec.meta['page_name'] = page_name;
    pages[page_id] = page_spec;
  }
  return pages;
}

module.exports = markdown_to_specdom;


/***/ }),

/***/ "./node_modules/specdom_tools/lib/mk_HTML.js":
/*!***************************************************!*\
  !*** ./node_modules/specdom_tools/lib/mk_HTML.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
* mk_html - Makes DOM Element from a ConfigDOM specs object
* @function
* @param  {object} specs ConfigDOM specs object
* @return {string} HTML
*/
var mk_html = function mk_html(specs, level){
  if( !specs ){
    specs = {};
  }
  if (!level ){
    level = 0;
  }

  ///////////////////////////////
  // Prep specs

  if( specs.constructor === Number ){
    specs = specs.toString();
  }

  if( specs.constructor === String ){
    //sdom = document.createTextNode(specs);
    specs = {
      tag: 'textNode',
      text: specs
    };
  }

  ///////////////////////////
  // make element

  var html_array = [];
  var html_line = ' '.repeat(level*2);

  if( specs.tag === 'elem' ) { // NODE ELEMENT
    html_line = specs.elem.outerhtml;
  // } else if( specs.textNode ) {
  } else if( specs.tag === 'textNode' ) {
    html_line = ' '.repeat(level * 2) + specs.text;
  } else { // CONFIG
    if( specs.tag ){
      html_line += '<'+specs.tag;
    } else {
      console.log('Elem type not found.', specs);
      return false;
    }

    if( specs.props ){
      for( var name in specs.props ){
        html_line += ' ' + name + '="' + specs.props[name] + '"';
      }
    }
    if( specs.style ){
      html_line += ' style="';
      for( var name in specs.style ){
        html_line += name + ':' + specs.style[name] + ';';
      }
      html_line += '"';
    }
    html_line += '>';

    html_array.push(html_line);
    html_line = ' '.repeat(level*2);

    if (specs.text ){
      html_line += specs.text;
      html_array.push(html_line);
      html_line = ' '.repeat(level*2);
    }

    if (specs.children && specs.children.length) {
      for (var i = 0; i < specs.children.length; i++) {
        var html_array_children = mk_html( specs.children[i], level+1 );
        html_array = html_array.concat(html_array_children);
      }
    }

    html_line += '</'+specs.tag+'>';
  }
  html_array.push(html_line);

  // html = html_array.join('\n');

  return html_array;
};


module.exports = mk_html;


/***/ }),

/***/ "./node_modules/specdom_tools/lib/specs_to_html.js":
/*!*********************************************************!*\
  !*** ./node_modules/specdom_tools/lib/specs_to_html.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var mk_HTML = __webpack_require__(/*! ./mk_HTML */ "./node_modules/specdom_tools/lib/mk_HTML.js");

/**
 * .
 * @exports
 */
module.exports = function(specs, settings){
  let title = settings.title || '';
  let format = settings.format || 'full';
  let css_files = settings.css_files || [];
  let js_files_pre = settings.js_files_pre || [];
  let js_files_post = settings.js_files_post || [];
  let final_lines = settings.final_lines || [];

  var html_array = [];

  if ( format !== 'body only' ){
    html_array = html_array.concat([
      '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" >',
      '<html>',
      '<head>',
      '<meta charset="UTF-8">',
      '<meta name="viewport" content="width=device-width,initial-scale=1">',
      `<title>${title}</title>`,
    ]);
    css_files.forEach( css_url => {
      html_array.push( `<link rel="stylesheet" href="${css_url}">` );
    });
    js_files_pre.forEach( js_url => {
      html_array.push( `<script type="text/javascript" src="${js_url}"></script>` );
    });
    html_array = html_array.concat([
      '</head>',
      '<body>',
    ]);
  }

  if ( specs.constuctor === Array ) {
    specs.forEach(spec => {
      html_array = html_array.concat( mk_HTML(specs) );
    });
  } else {
    html_array = html_array.concat( mk_HTML(specs) );
  }

  if ( format !== 'body only' ){
    js_files_post.forEach( js_url => {
      html_array.push( `<script type="text/javascript" src="${js_url}"></script>` );
    });
    final_lines.forEach( final_line => {
      html_array.push( final_line );
    });
    html_array = html_array.concat([
      '</body>',
      '</html>',
    ]);
  }

  format = format || 'compact';
  var html;
  if ( format === 'compact' || format === 'body only' ) {
    html_array = html_array.map(function (line) {
      return line.trim();
    });
    html = html_array.join('');
  } else {
    html = html_array.join('\n');
  }


  return html;
};


/***/ }),

/***/ "./node_modules/specdom_tools/lib/variate.js":
/*!***************************************************!*\
  !*** ./node_modules/specdom_tools/lib/variate.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function find_variables(specs,var_list){
  var_list = var_list || [];
  if ( specs.meta && specs.meta.var_type === 'variable' ){
    var_list.push( specs );
  }
  if ( specs.children ){
    specs.children.forEach( child_spec => {
      var_list = find_variables(child_spec,var_list);
    });
  }
  return var_list;
}

function set_value(obj,value,location_array){
  var destination = location_array.length === 1;
  if ( destination ) {
    obj[location_array[0]] = value;
    return obj;
  } else {
    obj[location_array[0]] = obj[location_array[0]] || {};
  }
  obj[location_array[0]] = set_value( obj[location_array[0]], value, location_array.slice(1) );
  return obj;
}

module.exports = function variate(specs,page_vars){
  page_vars = page_vars || {};
  var var_list = find_variables(specs);
  var_list.forEach( var_spec => {
    let var_id = var_spec.meta.var_id;
    let var_value = page_vars[var_id];
    if ( var_spec.meta.var_template && ( var_spec.meta.var_template.constructor = Array ) ){
      var_value = var_spec.meta.var_template.map( substring => {
        if ( page_vars[substring] !== undefined ){
          return page_vars[substring];
        } else {
          return substring;
        }
      }).join('');
    }
    let location_array = var_spec.meta.var_location.split('.');
    var_spec = set_value(var_spec,var_value,location_array);
    console.log(var_spec);

  });
  return specs;
};


/***/ }),

/***/ "./node_modules/state_manager/Actions.js":
/*!***********************************************!*\
  !*** ./node_modules/state_manager/Actions.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var mkAction = function(actions, action_name){
  return function(){
    actions.__dispatch({
      type: action_name,
      arguments: arguments
    });
  };
};

/* harmony default export */ __webpack_exports__["default"] = (function(store, custom_reducers){
  var actions = {
    __store: store,
    __dispatch: function(action_config){
      this.__store.dispatch(action_config);
    },
  };
  for( var action_name in custom_reducers ){
    actions[action_name] = mkAction(actions, action_name);
  }

  return actions;
});


/***/ }),

/***/ "./node_modules/state_manager/Reducer.js":
/*!***********************************************!*\
  !*** ./node_modules/state_manager/Reducer.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (function(custom_reducers){
  return function(state, action){
    state = state; // replace with deep copy
    if( custom_reducers[action.type] ){
      return custom_reducers[action.type](state, action);
    } else {
      console.log('NO ACTION HANDLER FOUND: ', action.type);
      return state;
    }
  };
});


/***/ }),

/***/ "./node_modules/state_manager/index.js":
/*!*********************************************!*\
  !*** ./node_modules/state_manager/index.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var specdom__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! specdom */ "./node_modules/specdom/index.js");
/* harmony import */ var specdom__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(specdom__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Actions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Actions */ "./node_modules/state_manager/Actions.js");
/* harmony import */ var _Reducer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Reducer */ "./node_modules/state_manager/Reducer.js");



//import clone from './clone';

var global = window || global;

/* harmony default export */ __webpack_exports__["default"] = (function(init_state, custom_reducers, cb){

  var reducer = Object(_Reducer__WEBPACK_IMPORTED_MODULE_2__["default"])(custom_reducers);

  var create_store = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js").createStore;
  var store = create_store(reducer, init_state);
  var actions = Object(_Actions__WEBPACK_IMPORTED_MODULE_1__["default"])(store, custom_reducers);

  /** anonymous function that runs when the store is updated. */
  store.subscribe(function(){
    var state = store.getState();
    cb(state, actions);
  });

  return actions;
});


/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js":
/*!****************************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var isOldIE = function isOldIE() {
  var memo;
  return function memorize() {
    if (typeof memo === 'undefined') {
      // Test for IE <= 9 as proposed by Browserhacks
      // @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
      // Tests for existence of standard globals is to allow style-loader
      // to operate correctly into non-standard environments
      // @see https://github.com/webpack-contrib/style-loader/issues/177
      memo = Boolean(window && document && document.all && !window.atob);
    }

    return memo;
  };
}();

var getTarget = function getTarget() {
  var memo = {};
  return function memorize(target) {
    if (typeof memo[target] === 'undefined') {
      var styleTarget = document.querySelector(target); // Special case to return head of iframe instead of iframe itself

      if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
        try {
          // This will throw an exception if access to iframe is blocked
          // due to cross-origin restrictions
          styleTarget = styleTarget.contentDocument.head;
        } catch (e) {
          // istanbul ignore next
          styleTarget = null;
        }
      }

      memo[target] = styleTarget;
    }

    return memo[target];
  };
}();

var stylesInDom = {};

function modulesToDom(moduleId, list, options) {
  for (var i = 0; i < list.length; i++) {
    var part = {
      css: list[i][1],
      media: list[i][2],
      sourceMap: list[i][3]
    };

    if (stylesInDom[moduleId][i]) {
      stylesInDom[moduleId][i](part);
    } else {
      stylesInDom[moduleId].push(addStyle(part, options));
    }
  }
}

function insertStyleElement(options) {
  var style = document.createElement('style');
  var attributes = options.attributes || {};

  if (typeof attributes.nonce === 'undefined') {
    var nonce =  true ? __webpack_require__.nc : undefined;

    if (nonce) {
      attributes.nonce = nonce;
    }
  }

  Object.keys(attributes).forEach(function (key) {
    style.setAttribute(key, attributes[key]);
  });

  if (typeof options.insert === 'function') {
    options.insert(style);
  } else {
    var target = getTarget(options.insert || 'head');

    if (!target) {
      throw new Error("Couldn't find a style target. This probably means that the value for the 'insert' parameter is invalid.");
    }

    target.appendChild(style);
  }

  return style;
}

function removeStyleElement(style) {
  // istanbul ignore if
  if (style.parentNode === null) {
    return false;
  }

  style.parentNode.removeChild(style);
}
/* istanbul ignore next  */


var replaceText = function replaceText() {
  var textStore = [];
  return function replace(index, replacement) {
    textStore[index] = replacement;
    return textStore.filter(Boolean).join('\n');
  };
}();

function applyToSingletonTag(style, index, remove, obj) {
  var css = remove ? '' : obj.css; // For old IE

  /* istanbul ignore if  */

  if (style.styleSheet) {
    style.styleSheet.cssText = replaceText(index, css);
  } else {
    var cssNode = document.createTextNode(css);
    var childNodes = style.childNodes;

    if (childNodes[index]) {
      style.removeChild(childNodes[index]);
    }

    if (childNodes.length) {
      style.insertBefore(cssNode, childNodes[index]);
    } else {
      style.appendChild(cssNode);
    }
  }
}

function applyToTag(style, options, obj) {
  var css = obj.css;
  var media = obj.media;
  var sourceMap = obj.sourceMap;

  if (media) {
    style.setAttribute('media', media);
  } else {
    style.removeAttribute('media');
  }

  if (sourceMap && btoa) {
    css += "\n/*# sourceMappingURL=data:application/json;base64,".concat(btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))), " */");
  } // For old IE

  /* istanbul ignore if  */


  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    while (style.firstChild) {
      style.removeChild(style.firstChild);
    }

    style.appendChild(document.createTextNode(css));
  }
}

var singleton = null;
var singletonCounter = 0;

function addStyle(obj, options) {
  var style;
  var update;
  var remove;

  if (options.singleton) {
    var styleIndex = singletonCounter++;
    style = singleton || (singleton = insertStyleElement(options));
    update = applyToSingletonTag.bind(null, style, styleIndex, false);
    remove = applyToSingletonTag.bind(null, style, styleIndex, true);
  } else {
    style = insertStyleElement(options);
    update = applyToTag.bind(null, style, options);

    remove = function remove() {
      removeStyleElement(style);
    };
  }

  update(obj);
  return function updateStyle(newObj) {
    if (newObj) {
      if (newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap) {
        return;
      }

      update(obj = newObj);
    } else {
      remove();
    }
  };
}

module.exports = function (moduleId, list, options) {
  options = options || {}; // Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
  // tags it will allow on a page

  if (!options.singleton && typeof options.singleton !== 'boolean') {
    options.singleton = isOldIE();
  }

  moduleId = options.base ? moduleId + options.base : moduleId;
  list = list || [];

  if (!stylesInDom[moduleId]) {
    stylesInDom[moduleId] = [];
  }

  modulesToDom(moduleId, list, options);
  return function update(newList) {
    newList = newList || [];

    if (Object.prototype.toString.call(newList) !== '[object Array]') {
      return;
    }

    if (!stylesInDom[moduleId]) {
      stylesInDom[moduleId] = [];
    }

    modulesToDom(moduleId, newList, options);

    for (var j = newList.length; j < stylesInDom[moduleId].length; j++) {
      stylesInDom[moduleId][j]();
    }

    stylesInDom[moduleId].length = newList.length;

    if (stylesInDom[moduleId].length === 0) {
      delete stylesInDom[moduleId];
    }
  };
};

/***/ }),

/***/ "./node_modules/symbol-observable/es/index.js":
/*!****************************************************!*\
  !*** ./node_modules/symbol-observable/es/index.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global, module) {/* harmony import */ var _ponyfill_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ponyfill.js */ "./node_modules/symbol-observable/es/ponyfill.js");
/* global window */


var root;

if (typeof self !== 'undefined') {
  root = self;
} else if (typeof window !== 'undefined') {
  root = window;
} else if (typeof global !== 'undefined') {
  root = global;
} else if (true) {
  root = module;
} else {}

var result = Object(_ponyfill_js__WEBPACK_IMPORTED_MODULE_0__["default"])(root);
/* harmony default export */ __webpack_exports__["default"] = (result);

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js"), __webpack_require__(/*! ./../../webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./node_modules/symbol-observable/es/ponyfill.js":
/*!*******************************************************!*\
  !*** ./node_modules/symbol-observable/es/ponyfill.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return symbolObservablePonyfill; });
function symbolObservablePonyfill(root) {
	var result;
	var Symbol = root.Symbol;

	if (typeof Symbol === 'function') {
		if (Symbol.observable) {
			result = Symbol.observable;
		} else {
			result = Symbol('observable');
			Symbol.observable = result;
		}
	} else {
		result = '@@observable';
	}

	return result;
};


/***/ }),

/***/ "./node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ "./node_modules/webpack/buildin/harmony-module.js":
/*!*******************************************!*\
  !*** (webpack)/buildin/harmony-module.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = function(originalModule) {
	if (!originalModule.webpackPolyfill) {
		var module = Object.create(originalModule);
		// module.parent = undefined by default
		if (!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function() {
				return module.i;
			}
		});
		Object.defineProperty(module, "exports", {
			enumerable: true
		});
		module.webpackPolyfill = 1;
	}
	return module;
};


/***/ }),

/***/ "./src/app.js":
/*!********************!*\
  !*** ./src/app.js ***!
  \********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../index */ "./index.js");
/* harmony import */ var specdom_tools__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! specdom_tools */ "./node_modules/specdom_tools/index.js");
/* harmony import */ var specdom_tools__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(specdom_tools__WEBPACK_IMPORTED_MODULE_1__);



let http_request = new XMLHttpRequest();
if (!http_request) {
  alert('Cannot create an XMLHTTP instance');
} else {
  http_request.onreadystatechange = load_pages;
  http_request.open('GET', 'pages.json');
  http_request.send();
}
function load_pages() {
  if (http_request.readyState === XMLHttpRequest.DONE) {
    if (http_request.status === 200) {
      let pages = JSON.parse(http_request.responseText);
      for (let page_id in pages) {
        pages[page_id] = Object(specdom_tools__WEBPACK_IMPORTED_MODULE_1__["expand"])(pages[page_id]);
      }
      console.log('Loading Dynatic...');
      //dynatic(pages, settings);
      Object(_index__WEBPACK_IMPORTED_MODULE_0__["default"])(pages);
    } else {
      alert('There was a problem with the request.');
    }
  }
}



/***/ }),

/***/ "./src/mk_page_specs.js":
/*!******************************!*\
  !*** ./src/mk_page_specs.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (function (state) {

  var title = state.title;
  var menu_items = state.menu_items;
  var selected_page_id = state.selected_page_id;
  var page_content = state.page_content;

  var menu_specs = {
    tag: 'div',
    props: {
      id: 'menu'
    },
    children: []
  };

  menu_specs.children.push({
    tag: 'span',
    text: '[',
    props: {
      class: 'menu_bracket',
    }
  });

  menu_items.forEach(function (menu_item) {
    //var name = f.pretty_name(menu_item.name);
    var name = menu_item.name.trim();
    var selected = menu_item.href.slice(2) === selected_page_id;
    if (selected_page_id === 'default' && menu_item.href === '#/') {
      selected = true;
    }
    // console.log('menu_item.href',menu_item.href,selected_page_id,selected);
    //console.log('selected', page_id, selected_page_id, selected);
    menu_specs.children.push({
      tag: selected ? 'span' : 'a',
      text: name,
      props: {
        class: selected ? 'site_title titlebar_link titlebar_link_selected' : 'site_title titlebar_link',
        href: menu_item.href
      }
    });
  });

  menu_specs.children.push({
    tag: 'span',
    text: ']',
    props: {
      class: 'site_title menu_bracket',
    }
  });

  title = title.constructor === Array ? title : [title];
  var titlebar_content = {
    tag: 'div',
    props: {
      class: 'titlebar_content'
    },
    children: [
      {
        tag: 'div',
        id: 'site_title',
        class: 'site_title_section',
        _c: title.map(subtitle => {
          return {
            _: 'a',
            _t: subtitle,
            _p: { href: '#/' }
          };
        }),
      },
      menu_specs,
    ]
  };

  var specs = {
    tag: 'div',
    children: [
      {
        tag: 'div',
        props: {
          class: 'titlebar'
        },
        children: [titlebar_content]
      },
      {
        tag: 'div',
        props: {
          class: 'transition'
        }
      },
      page_content
    ]
  };

  return specs;
});


/***/ }),

/***/ "./src/reducers.js":
/*!*************************!*\
  !*** ./src/reducers.js ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _mk_page_specs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./mk_page_specs */ "./src/mk_page_specs.js");
const variate = __webpack_require__(/*! specdom_tools */ "./node_modules/specdom_tools/index.js").variate;

/* harmony default export */ __webpack_exports__["default"] = ({
  load_page: function (state, action) {
    var selection = action.arguments[0];
    console.log('selection: ', selection);
    if (!selection) {
      console.log('re-ROUTING... to home');
      selection = [state.default_page_name];
      console.log('selection: ', selection);
    }
    selection = selection.filter(string => {
      return string !== '';
    });
    var selected_page_id = state.selected_page_id = selection.join('/');
    var selected_location = selection.slice(0, selection.length - 1).join(' / ');
    if (!selected_location) {
      selected_location = '/';
    }
    console.log('ROUTING... ' + selected_page_id);
    var page = state.pages[selected_page_id];
    // console.log(state.pages);
    // console.log(page);
    var page_content;
    if (!page && !state.pages[404]) {
      page_content = {
        tag: 'p',
        text: '404 page not found'
      };
    } else if (!page) {
      page_content = state.pages[404];
    } else {
      page_content = page;
    }
    state.page_content = page_content;
    var specs = Object(_mk_page_specs__WEBPACK_IMPORTED_MODULE_0__["default"])(state);
    let page_vars = state.page_content.meta;
    specs = variate(specs, page_vars);
    console.log('SPECS', specs);
    state.specs = specs;
    return state;
  },
});


/***/ }),

/***/ 0:
/*!**************************!*\
  !*** multi ./src/app.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./src/app.js */"./src/app.js");


/***/ })

/******/ });
//# sourceMappingURL=app.js.map